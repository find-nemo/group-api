import { Container } from "inversify";

import { PluginManager } from "./plugins.config";
import { InterfaceErrorHandlerPlugin } from "./plugins/error-handler.interface";
import { ErrorHandlerPlugin } from "./plugins/error-handler.plugin";
import { InterfaceRequestValidationMiddleware } from "./plugins/request-validation.interface";
import { RequestValidationMiddleware } from "./plugins/request-validation.plugin";
import { InterfaceSecretManagerPlugin } from "./plugins/secret-manager.interface";
import { SecretManagerPlugin } from "./plugins/secret-manager.plugin";
import { InterfaceSequelizePlugin } from "./plugins/sequelize.interface";
import { SequelizePlugin } from "./plugins/sequelize.plugin";
import { ChildGroupController } from "./server/controllers/child-group.controller";
import { DriverGroupController } from "./server/controllers/driver-group.controller";
import { DriverSchoolController } from "./server/controllers/driver-school.controller";
import { GroupController } from "./server/controllers/group.controller";
import { HealthController } from "./server/controllers/health.controller";
import { PickupDropoffController } from "./server/controllers/pickup-dropoff.controller";
import { SchoolController } from "./server/controllers/school.controller";
import { SwaggerController } from "./server/controllers/swagger.controller";
import { TripController } from "./server/controllers/trip.controller";
import { FirebaseTokenMiddleware } from "./server/middlewares/firebase-token.middleware";
import { SYMBOLS } from "./server/models/error-symbol";
import { ChildGroupDataAccess } from "./server/repository/child-group.da";
import { ChildGroupDataAccessInterface } from "./server/repository/child-group.interface";
import { ChildDataAccess } from "./server/repository/child.da";
import { ChildDataAccessInterface } from "./server/repository/child.interface";
import { DeviceDataAccess } from "./server/repository/device.da";
import { DeviceDataAccessInterface } from "./server/repository/device.interface";
import { DriverGroupDataAccess } from "./server/repository/driver-group.da";
import { DriverGroupDataAccessInterface } from "./server/repository/driver-group.interface";
import { GroupDataAccess } from "./server/repository/group.da";
import { GroupDataAccessInterface } from "./server/repository/group.interface";
import { PickupDropoffDataAccess } from "./server/repository/pickup-dropoff.da";
import { PickupDropoffDataAccessInterface } from "./server/repository/pickup-dropoff.interface";
import { SchoolDataAccess } from "./server/repository/school.da";
import { SchoolDataAccessInterface } from "./server/repository/school.interface";
import { TripDataAccess } from "./server/repository/trip.da";
import { TripDataAccessInterface } from "./server/repository/trip.interface";
import { UserTypeDataAccess } from "./server/repository/user-type.da";
import { UserTypeDataAccessInterface } from "./server/repository/user-type.interface";
import { UserDataAccess } from "./server/repository/user.da";
import { UserDataAccessInterface } from "./server/repository/user.interface";
import { ChildGroupServiceInterface } from "./server/services/child-group.interface";
import { ChildGroupService } from "./server/services/child-group.service";
import { ChildServiceInterface } from "./server/services/child.interface";
import { ChildService } from "./server/services/child.service";
import { CloudMessagingInterface } from "./server/services/cloud-messaging.interface";
import { CloudMessagingService } from "./server/services/cloud-messaging.service";
import { DeviceServiceInterface } from "./server/services/device.interface";
import { DeviceService } from "./server/services/device.service";
import { DistanceServiceInterface } from "./server/services/distance.interface";
import { DistanceService } from "./server/services/distance.service";
import { DriverGroupServiceInterface } from "./server/services/driver-group.interface";
import { DriverGroupService } from "./server/services/driver-group.service";
import { FirestoreServiceInterface } from "./server/services/firestore.interface";
import { FirestoreService } from "./server/services/firestore.service";
import { GoogleMapsServiceInterface } from "./server/services/google-maps.interface";
import { GoogleMapsService } from "./server/services/google-maps.service";
import { GroupServiceInterface } from "./server/services/group.interface";
import { GroupService } from "./server/services/group.service";
import { HereMapsServiceInterface } from "./server/services/here-maps.interface";
import { HereMapsService } from "./server/services/here-maps.service";
import { PickupDropoffServiceInterface } from "./server/services/pickup-dropoff.interface";
import { PickupDropoffService } from "./server/services/pickup-dropoff.service";
import { SchoolServiceInterface } from "./server/services/school.interface";
import { SchoolService } from "./server/services/school.service";
import { TripServiceInterface } from "./server/services/trip.interface";
import { TripService } from "./server/services/trip.service";
import { UserServiceInterface } from "./server/services/user.interface";
import { UserService } from "./server/services/user.service";

const appContainer = new Container();
appContainer
  .bind<PluginManager>(nameof<PluginManager>())
  .to(PluginManager)
  .inSingletonScope();
appContainer
  .bind<InterfaceErrorHandlerPlugin>(nameof<InterfaceErrorHandlerPlugin>())
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  .toFactory<InterfaceErrorHandlerPlugin>((): any => {
    return (name: string): ErrorHandlerPlugin => {
      name = name || "UNKNOWN";
      const service = appContainer.get<ErrorHandlerPlugin>(
        SYMBOLS.DiagnosticsInstance
      );
      service.setName(name);
      return service;
    };
  });
appContainer
  .bind<InterfaceErrorHandlerPlugin>(SYMBOLS.DiagnosticsInstance)
  .to(ErrorHandlerPlugin)
  .inTransientScope();
appContainer
  .bind<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  )
  .to(RequestValidationMiddleware)
  .inTransientScope();
appContainer
  .bind<SwaggerController>(nameof<SwaggerController>())
  .to(SwaggerController);
appContainer
  .bind<HealthController>(nameof<HealthController>())
  .to(HealthController);
appContainer
  .bind<FirebaseTokenMiddleware>(nameof<FirebaseTokenMiddleware>())
  .to(FirebaseTokenMiddleware);
appContainer
  .bind<InterfaceSecretManagerPlugin>(nameof<InterfaceSecretManagerPlugin>())
  .to(SecretManagerPlugin)
  .inSingletonScope();
appContainer
  .bind<InterfaceSequelizePlugin>(nameof<InterfaceSequelizePlugin>())
  .to(SequelizePlugin)
  .inSingletonScope();
appContainer
  .bind<DriverGroupDataAccessInterface>(
    nameof<DriverGroupDataAccessInterface>()
  )
  .to(DriverGroupDataAccess)
  .inSingletonScope();
appContainer
  .bind<DriverGroupServiceInterface>(nameof<DriverGroupServiceInterface>())
  .to(DriverGroupService)
  .inSingletonScope();
appContainer
  .bind<DriverGroupController>(nameof<DriverGroupController>())
  .to(DriverGroupController);
appContainer
  .bind<DriverSchoolController>(nameof<DriverSchoolController>())
  .to(DriverSchoolController);
appContainer
  .bind<TripDataAccessInterface>(nameof<TripDataAccessInterface>())
  .to(TripDataAccess)
  .inSingletonScope();
appContainer
  .bind<TripServiceInterface>(nameof<TripServiceInterface>())
  .to(TripService)
  .inSingletonScope();
appContainer.bind<TripController>(nameof<TripController>()).to(TripController);
appContainer
  .bind<PickupDropoffDataAccessInterface>(
    nameof<PickupDropoffDataAccessInterface>()
  )
  .to(PickupDropoffDataAccess)
  .inSingletonScope();
appContainer
  .bind<PickupDropoffServiceInterface>(nameof<PickupDropoffServiceInterface>())
  .to(PickupDropoffService)
  .inSingletonScope();
appContainer
  .bind<PickupDropoffController>(nameof<PickupDropoffController>())
  .to(PickupDropoffController);
appContainer
  .bind<GroupDataAccessInterface>(nameof<GroupDataAccessInterface>())
  .to(GroupDataAccess)
  .inSingletonScope();
appContainer
  .bind<GroupServiceInterface>(nameof<GroupServiceInterface>())
  .to(GroupService)
  .inSingletonScope();
appContainer
  .bind<GroupController>(nameof<GroupController>())
  .to(GroupController);
appContainer
  .bind<ChildGroupDataAccessInterface>(nameof<ChildGroupDataAccessInterface>())
  .to(ChildGroupDataAccess)
  .inSingletonScope();
appContainer
  .bind<ChildGroupServiceInterface>(nameof<ChildGroupServiceInterface>())
  .to(ChildGroupService)
  .inSingletonScope();
appContainer
  .bind<ChildGroupController>(nameof<ChildGroupController>())
  .to(ChildGroupController);
appContainer
  .bind<CloudMessagingInterface>(nameof<CloudMessagingInterface>())
  .to(CloudMessagingService)
  .inSingletonScope();
appContainer
  .bind<DeviceDataAccessInterface>(nameof<DeviceDataAccessInterface>())
  .to(DeviceDataAccess)
  .inSingletonScope();
appContainer
  .bind<DeviceServiceInterface>(nameof<DeviceServiceInterface>())
  .to(DeviceService)
  .inSingletonScope();
appContainer
  .bind<FirestoreServiceInterface>(nameof<FirestoreServiceInterface>())
  .to(FirestoreService)
  .inSingletonScope();
appContainer
  .bind<UserTypeDataAccessInterface>(nameof<UserTypeDataAccessInterface>())
  .to(UserTypeDataAccess)
  .inSingletonScope();
appContainer
  .bind<UserDataAccessInterface>(nameof<UserDataAccessInterface>())
  .to(UserDataAccess)
  .inSingletonScope();
appContainer
  .bind<UserServiceInterface>(nameof<UserServiceInterface>())
  .to(UserService)
  .inSingletonScope();
appContainer
  .bind<HereMapsServiceInterface>(nameof<HereMapsServiceInterface>())
  .to(HereMapsService)
  .inSingletonScope();
appContainer
  .bind<GoogleMapsServiceInterface>(nameof<GoogleMapsServiceInterface>())
  .to(GoogleMapsService)
  .inSingletonScope();
appContainer
  .bind<DistanceServiceInterface>(nameof<DistanceServiceInterface>())
  .to(DistanceService)
  .inSingletonScope();
appContainer
  .bind<SchoolDataAccessInterface>(nameof<SchoolDataAccessInterface>())
  .to(SchoolDataAccess)
  .inSingletonScope();
appContainer
  .bind<SchoolServiceInterface>(nameof<SchoolServiceInterface>())
  .to(SchoolService)
  .inSingletonScope();
appContainer
  .bind<SchoolController>(nameof<SchoolController>())
  .to(SchoolController);
appContainer
  .bind<ChildDataAccessInterface>(nameof<ChildDataAccessInterface>())
  .to(ChildDataAccess)
  .inSingletonScope();
appContainer
  .bind<ChildServiceInterface>(nameof<ChildServiceInterface>())
  .to(ChildService)
  .inSingletonScope();
export { appContainer };
