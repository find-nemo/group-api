import { Router } from 'express';
import { DriverGroup } from '../server/models/driver-group';

import { appContainer } from '../inversify.config';
import { InterfaceRequestValidationMiddleware } from '../plugins/request-validation.interface';
import { ChildGroupController } from '../server/controllers/child-group.controller';
import { DriverGroupController } from '../server/controllers/driver-group.controller';
import { GroupController } from '../server/controllers/group.controller';
import { HealthController } from '../server/controllers/health.controller';
import { PickupDropoffController } from '../server/controllers/pickup-dropoff.controller';
import { SchoolController } from '../server/controllers/school.controller';
import { SwaggerController } from '../server/controllers/swagger.controller';
import { TripController } from '../server/controllers/trip.controller';
import { ChildGroup } from '../server/models/child-group';
import { Group } from '../server/models/group';
import { PickupDropoff } from '../server/models/pickup-dropoff';
import { School } from '../server/models/school';
import { Trip } from '../server/models/trip';
import { DriverSchoolController } from '../server/controllers/driver-school.controller';
import { Child } from '../server/models/child';
import { User } from '../server/models/user';

const routes = Router();

const swaggerController = appContainer.get<SwaggerController>(
  nameof<SwaggerController>()
);
const healthController = appContainer.get<HealthController>(
  nameof<HealthController>()
);

const requestValidationMiddleware =
  appContainer.get<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  );

const validationMiddleware =
  requestValidationMiddleware.validationMiddleware.bind(
    requestValidationMiddleware
  );

const driverGroupController = appContainer.get<DriverGroupController>(
  nameof<DriverGroupController>()
);

const driverSchoolController = appContainer.get<DriverSchoolController>(
  nameof<DriverSchoolController>()
);

const groupController = appContainer.get<GroupController>(
  nameof<GroupController>()
);

const tripController = appContainer.get<TripController>(
  nameof<TripController>()
);

const pickupDropoffController = appContainer.get<PickupDropoffController>(
  nameof<PickupDropoffController>()
);

const childGroupController = appContainer.get<ChildGroupController>(
  nameof<ChildGroupController>()
);

const schoolController = appContainer.get<SchoolController>(
  nameof<SchoolController>()
);

routes.get('/', healthController.getHealth.bind(healthController));

routes.get('/health', healthController.getHealth.bind(healthController));

routes.get('/api', swaggerController.getDocs.bind(swaggerController));

/**
 * @swagger
 * /driver-groups/:
 *  get:
 *    description: Get groups by driverId
 *    summary: Returns array of group by driverId
 *    tags: [DriverGroup]
 *    responses:
 *      200:
 *        description: Returns array of group by driverId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/DriverGroupWithGroupAndUserResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/driver-groups/',
  driverGroupController.getByDriverId.bind(driverGroupController)
);

/**
 * @swagger
 * /driver-groups/drivers/group/{groupId}/:
 *  get:
 *    description: Get groups by driverId
 *    summary: Returns array of drivers by groupId
 *    tags: [DriverGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns array of drivers by groupId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/DriverGroupWithUserResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/driver-groups/drivers/group/:groupId/',
  [
    validationMiddleware('params', DriverGroup, {
      validator: { groups: ['getDriversByGroupId'] },
    }),
  ],
  driverGroupController.getDriversByGroupId.bind(driverGroupController)
);

/**
 * @swagger
 * /driver-groups/:
 *  post:
 *    description: Add drivers to the group
 *    summary: Returns boolean indicating whether the driver was added to the group
 *    tags: [DriverGroup]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/AddDriverToGroupBody'
 *    responses:
 *      200:
 *        description: Returns boolean indicating whether the driver was added to the group
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/DriverGroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/driver-groups/',
  [
    validationMiddleware('body', DriverGroup, {
      validator: { groups: ['addDriverToGroup'] },
    }),
  ],
  driverGroupController.addDriverToGroup.bind(driverGroupController)
);

/**
 * @swagger
 * /driver-groups/driver/{driverId}/group/{groupId}/:
 *  delete:
 *    description: Delete driver from group
 *    summary: Returns boolean indicating whether the driver was deleted from the group
 *    tags: [DriverGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *      - name: driverId
 *        description: Driver id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns boolean indicating whether the driver was deleted from the group
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/DeleteRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  '/driver-groups/driver/:driverId/group/:groupId',
  [
    validationMiddleware('params', DriverGroup, {
      validator: { groups: ['deleteDriverFromGroup'] },
    }),
  ],
  driverGroupController.deleteDriverFromGroup.bind(driverGroupController)
);

/**
 * @swagger
 * /group/:
 *  post:
 *    description: Create group
 *    summary: Returns newly created group
 *    tags: [Group]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateGroupBody'
 *    responses:
 *      200:
 *        description: Returns newly created group
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/GroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/group/',
  [
    validationMiddleware('body', Group, {
      validator: { groups: ['createGroup'] },
    }),
  ],
  groupController.create.bind(groupController)
);

/**
 * @swagger
 * /group/publicId/{publicId}:
 *  get:
 *    description: Get group by publicIp
 *    summary: Returns a group by publicIp
 *    tags: [Group]
 *    parameters:
 *      - name: publicId
 *        description: Public id
 *        in: path
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns a group by publicIp
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/GroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/group/publicId/:publicId/',
  [
    validationMiddleware('params', Group, {
      validator: { groups: ['getByPublicId'] },
    }),
  ],
  groupController.getByPublicId.bind(groupController)
);

/**
 * @swagger
 * /groups/phoneNumber/{phoneNumber}/:
 *  get:
 *    description: Get groups by phoneNumber
 *    summary: Returns a list of group by phoneNumber
 *    tags: [Group]
 *    parameters:
 *      - name: phoneNumber
 *        description: Phone Number with country prefix
 *        in: path
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns a list of group by phoneNumber
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/GroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/groups/phoneNumber/:phoneNumber/',
  [
    validationMiddleware('params', User, {
      validator: { groups: ['getGroupByPhoneNumber'] },
    }),
  ],
  driverGroupController.getByPhoneNumber.bind(driverGroupController)
);

/**
 * @swagger
 * /group/:
 *  put:
 *    description: Update group
 *    summary: Returns updated group
 *    tags: [Group]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UpdateGroupBody'
 *    responses:
 *      200:
 *        description: Returns number of items updated
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/group/',
  [
    validationMiddleware('body', Group, {
      validator: { groups: ['updateGroup'] },
    }),
  ],
  groupController.update.bind(groupController)
);

/**
 * @swagger
 * /group/{id}/:
 *  delete:
 *    description: Delete group
 *    summary: Returns number of groups deleted
 *    tags: [Group]
 *    parameters:
 *      - name: id
 *        description: Group id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns number of groups deleted
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeleteRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  '/group/:id',
  [
    validationMiddleware('params', Group, {
      validator: { groups: ['deleteGroupById'] },
    }),
  ],
  groupController.deleteByGroupId.bind(groupController)
);

/**
 * @swagger
 * /trips/groups/{groupId}/:
 *  get:
 *    description: Get trips by groupId
 *    summary: Returns array of trips by groupId
 *    tags: [Trip]
 *    parameters:
 *      - name: groupId
 *        description: Group id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns array of trip by groupId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/TripWithGroupAndDeviceAndUserResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/trips/groups/:groupId/',
  [
    validationMiddleware('params', Trip, {
      validator: { groups: ['getTripsByGroupId'] },
    }),
  ],
  tripController.getByGroupId.bind(tripController)
);

/**
 * @swagger
 * /driver-trips/latest/:
 *  get:
 *    description: Get latest trips by driver
 *    summary: Returns array of trips by driver
 *    tags: [Trip]
 *    responses:
 *      200:
 *        description: Returns array of trip by driver
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/TripResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/driver-trips/latest/',
  tripController.getLatestTripByDriverId.bind(tripController)
);

/**
 * @swagger
 * /child-trips/latest/:
 *  get:
 *    description: Get latest trips for each child by user
 *    summary: Returns array of latest trip for each child by user
 *    tags: [Trip]
 *    responses:
 *      200:
 *        description: Returns array of trip by groupId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/TripWithGroupAndDeviceAndUserResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/child-trips/latest/',
  tripController.getLatestTripsByUserId.bind(tripController)
);

/**
 * @swagger
 * /trip/start/:
 *  post:
 *    description: Start a new trip
 *    summary: Return newly created trip
 *    tags: [Trip]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/StartTripBody'
 *    responses:
 *      200:
 *        description: Return newly created trip
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TripResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/trip/start/',
  [
    validationMiddleware('body', Trip, {
      validator: { groups: ['start'] },
    }),
  ],
  tripController.start.bind(tripController)
);

/**
 * @swagger
 * /trip/end/:
 *  put:
 *    description: End a trip
 *    summary: Return number of trips ended
 *    tags: [Trip]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/EndTripBody'
 *    responses:
 *      200:
 *        description: Return number of trips ended
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/trip/end/',
  [
    validationMiddleware('body', Trip, {
      validator: { groups: ['end'] },
    }),
  ],
  tripController.end.bind(tripController)
);

/**
 * @swagger
 * /trip/location/:
 *  patch:
 *    description: Update trip current driver location
 *    summary: Return number of trips updated
 *    tags: [Trip]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/DriverLocationUpdateTripBody'
 *    responses:
 *      200:
 *        description: Return number of trips updated
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  '/trip/location/',
  [
    validationMiddleware('body', Trip, {
      validator: { groups: ['update_location'] },
    }),
  ],
  tripController.updateTripDriverCurrentLocation.bind(tripController)
);

/**
 * @swagger
 * /inactive/trips/:
 *  patch:
 *    description: End inactive trip that are not updated since last 2 hours
 *    summary: Return number of trips updated
 *    tags: [Trip]
 *    responses:
 *      200:
 *        description: End inactive trip that are not updated since last 2 hours
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  '/inactive/trips/',
  tripController.endInactiveTrips.bind(tripController)
);

/**
 * @swagger
 * /distance/active/trips/:
 *  get:
 *    description: Update active trip distance
 *    summary: Update distance and duration for all the active trips
 *    tags: [Trip]
 *    responses:
 *      200:
 *        description: Updates distance and duration for all the active trips in firestore database
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/distance/active/trips/',
  tripController.updateActiveTripsDistanceAndDuration.bind(tripController)
);

/**
 * @swagger
 * /pickup-dropoffs/trips/{tripId}/:
 *  get:
 *    description: Get pickup-dropoff by tripId
 *    summary: Returns array of pickup-dropoff by tripId
 *    tags: [PickupDropoff]
 *    parameters:
 *      - name: tripId
 *        description: Trip id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns array of pickup-dropoff by tripId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/PickupDropoffWithTripAndChildAndDriverResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/pickup-dropoffs/trips/:tripId/',
  [
    validationMiddleware('params', PickupDropoff, {
      validator: { groups: ['getPickupDropoffByTripId'] },
    }),
  ],
  pickupDropoffController.getByTripId.bind(pickupDropoffController)
);

/**
 * @swagger
 * /trips/pickup/:
 *  post:
 *    description: Pickup childrens by tripId
 *    summary: Returns array of pickupdropoff
 *    tags: [PickupDropoff]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreatePickupBody'
 *    responses:
 *      200:
 *        description: Returns array of pickupdropoff
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/PickupDropoffResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/trips/pickup/',
  [
    validationMiddleware('body', PickupDropoff, {
      validator: { groups: ['pickupChild'] },
    }),
  ],
  pickupDropoffController.pickupChild.bind(pickupDropoffController)
);

/**
 * @swagger
 * /trips/dropoff/:
 *  put:
 *    description: Dropoff childrens by tripId
 *    summary: Returns number of children Dropoff
 *    tags: [PickupDropoff]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateDropoffBody'
 *    responses:
 *      200:
 *        description: Returns number of children Dropoff
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/trips/dropoff/',
  [
    validationMiddleware('body', PickupDropoff, {
      validator: { groups: ['dropoffChild'] },
    }),
  ],
  pickupDropoffController.dropoffChild.bind(pickupDropoffController)
);

/**
 * @swagger
 * /trips/not-absent/:
 *  put:
 *    description: Delete absent childrens by tripId
 *    summary: Returns number of children marked not absent
 *    tags: [PickupDropoff]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateNotAbsentBody'
 *    responses:
 *      200:
 *        description: Returns number of children marked not absent
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/trips/not-absent/',
  [
    validationMiddleware('body', PickupDropoff, {
      validator: { groups: ['absentChild'] },
    }),
  ],
  pickupDropoffController.notAbsentChild.bind(pickupDropoffController)
);

/**
 * @swagger
 * /trips/absent/:
 *  post:
 *    description: Absent childrens by tripId
 *    summary: Returns array of pickupdropoff
 *    tags: [PickupDropoff]
 *    requestBody:
 *      description: Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateAbsentBody'
 *    responses:
 *      200:
 *        description: Returns array of pickupdropoff
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/PickupDropoffResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/trips/absent/',
  [
    validationMiddleware('body', PickupDropoff, {
      validator: { groups: ['absentChild'] },
    }),
  ],
  pickupDropoffController.absentChild.bind(pickupDropoffController)
);

/**
 * @swagger
 * /childs/groups/{groupId}/:
 *  get:
 *    description: Get childs by groupId
 *    summary: Returns array of child-group by groupId
 *    tags: [ChildGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns array of child-group by groupId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ChildGroupWithChildWithGroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/childs/groups/:groupId',
  [
    validationMiddleware('params', ChildGroup, {
      validator: { groups: ['getChildsByGroupId'] },
    }),
  ],
  childGroupController.getByGroupId.bind(childGroupController)
);

/**
 * @swagger
 * /childs/groups/{groupId}/add/:
 *  post:
 *    description: Add childs to group
 *    summary: Returns added child to the group
 *    tags: [ChildGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    requestBody:
 *      description: ChildIds body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/ChildGroupChildIdsBody'
 *    responses:
 *      200:
 *        description: Returns added child to the group
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ChildGroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/childs/groups/:groupId/add/',
  [
    validationMiddleware('params', ChildGroup, {
      validator: { groups: ['getChildsByGroupId'] },
    }),
    validationMiddleware('body', ChildGroup, {
      validator: {
        groups: ['addChildToGroup'],
      },
    }),
  ],
  childGroupController.addChildsToGroup.bind(childGroupController)
);

/**
 * @swagger
 * /childs/groups/{groupId}/:
 *  post:
 *    description: Add child to group
 *    summary: Returns added child to the group
 *    tags: [ChildGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    requestBody:
 *      description: Child body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateParentChildBody'
 *    responses:
 *      200:
 *        description: Returns added child to the group
 *        content:
 *          application/json:
 *            schema:
 *               $ref: '#/components/schemas/ChildResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/childs/groups/:groupId/',
  [
    validationMiddleware('params', ChildGroup, {
      validator: { groups: ['getChildsByGroupId'] },
    }),
    validationMiddleware('body', Child, {
      validator: {
        groups: ['addParentChildToGroup'],
      },
    }),
  ],
  childGroupController.addParentChildToGroup.bind(childGroupController)
);

/**
 * @swagger
 * /childs/groups/{groupId}/delete/:
 *  put:
 *    description: Removes childs from group
 *    summary: Returns number of child removed from the group
 *    tags: [ChildGroup]
 *    parameters:
 *      - name: groupId
 *        description: Group Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    requestBody:
 *      description: ChildIds body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/ChildGroupChildIdsBody'
 *    responses:
 *      200:
 *        description: Returns number of child removed from the group
 *        content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/childs/groups/:groupId/delete/',
  [
    validationMiddleware('params', ChildGroup, {
      validator: { groups: ['getChildsByGroupId'] },
    }),
    validationMiddleware('body', ChildGroup, {
      validator: {
        groups: ['removeChildFromGroup'],
      },
    }),
  ],
  childGroupController.removeChildFromGroup.bind(childGroupController)
);

/**
 * @swagger
 * /child-groups/{childId}/:
 *  get:
 *    description: Get childs by childId
 *    summary: Returns array of child-group by childId
 *    tags: [ChildGroup]
 *    parameters:
 *      - name: childId
 *        description: ChildId Id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *    responses:
 *      200:
 *        description: Returns array of child-group by childId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ChildGroupWithChildWithGroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/child-groups/:childId/',
  [
    validationMiddleware('params', ChildGroup, {
      validator: { groups: ['getChildsByChildId'] },
    }),
  ],
  childGroupController.getByChildId.bind(childGroupController)
);

/**
 * @swagger
 * /child-groups/:
 *  get:
 *    description: Get childs by user
 *    summary: Returns array of child-group by user
 *    tags: [ChildGroup]
 *    responses:
 *      200:
 *        description: Returns array of child-group by user
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ChildGroupWithChildWithGroupResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/child-groups/',
  childGroupController.getByUserId.bind(childGroupController)
);

/**
 * @swagger
 * /child-groups/monthly-fees/:
 *  put:
 *    description: Update the child-group monthly fees
 *    summary: Returns number of child-group items updated
 *    tags: [ChildGroup]
 *    requestBody:
 *      description: Child Group body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/ChildGroupMonthlyFeesBody'
 *    responses:
 *      200:
 *        description: Returns number of child-group items updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UpdateRecordsResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  '/child-groups/monthly-fees/',
  validationMiddleware('body', ChildGroup, {
    validator: { groups: ['updateChildGroupMonthlyFees'] },
  }),
  childGroupController.updateChildGroupMonthlyFees.bind(childGroupController)
);

/**
 * @swagger
 * /driver-schools/:
 *  get:
 *    description: Get schools by driverId
 *    summary: Returns array of schools by driverId
 *    tags: [School]
 *    parameters:
 *      - name: includeCreator
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *      - name: includeAddress
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns array of schools by driverId
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/SchoolResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/driver-schools/',
  [
    validationMiddleware('params', School, {
      validator: { groups: ['getDriverSchools'] },
    }),
  ],
  driverSchoolController.getDriverSchools.bind(driverSchoolController)
);

/**
 * @swagger
 * /school/:
 *  get:
 *    description: Get schools by name
 *    summary: Returns array of school by name
 *    tags: [School]
 *    parameters:
 *      - name: fullName
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *      - name: includeCreator
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *      - name: includeAddress
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns array of School response
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/SchoolResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/school/',
  [
    validationMiddleware('params', School, {
      validator: { groups: ['searchByName'] },
    }),
  ],
  schoolController.searchByName.bind(schoolController)
);

/**
 * @swagger
 * /school/{id}/:
 *  get:
 *    description: Get schools by id
 *    summary: Returns array of school by id
 *    tags: [School]
 *    parameters:
 *      - name: id
 *        description: School id
 *        in: path
 *        required: true
 *        schema:
 *          type: integer
 *      - name: includeCreator
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *      - name: includeAddress
 *        description: School name
 *        in: query
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: Returns School response
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SchoolResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/school/:id/',
  [
    validationMiddleware('params', School, {
      validator: { groups: ['searchById'] },
    }),
  ],
  schoolController.searchById.bind(schoolController)
);

/**
 * @swagger
 * /school/:
 *  post:
 *    description: Create a new school
 *    summary: Returns a newly created school
 *    tags: [School]
 *    requestBody:
 *      description: School body
 *      required: true
 *      content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateSchoolBody'
 *    responses:
 *      200:
 *        description: Returns array of School response
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/SchoolResponse'
 *      default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/school/',
  [
    validationMiddleware('body', School, {
      validator: { groups: ['create'] },
    }),
  ],
  schoolController.create.bind(schoolController)
);

export { routes };
