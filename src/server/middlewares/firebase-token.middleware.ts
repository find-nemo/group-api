import { NextFunction, Request, Response } from 'express';
import * as admin from 'firebase-admin';
import { inject, injectable } from 'inversify';
import moment from 'moment';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { InterfaceSecretManagerPlugin } from '../../plugins/secret-manager.interface';
import { FirebaseSecret } from '../models/firebase';
import {
  RequestWithPhoneNumber,
  RequestWithToken,
} from '../models/request-validation';
import { SecretKeys } from '../models/secrets';
import { logger } from '../utils/logger';

@injectable()
export class FirebaseTokenMiddleware {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<InterfaceSecretManagerPlugin>())
    private _secretsManagerPlugin: InterfaceSecretManagerPlugin
  ) {
    this._error = errorFactory(nameof<FirebaseTokenMiddleware>());
    this._secretsManagerPlugin
      .getSecretJson(SecretKeys.firebase)
      .then((credentials: FirebaseSecret) => {
        admin.initializeApp({
          credential: admin.credential.cert({
            projectId: credentials.project_id,
            clientEmail: credentials.client_email,
            privateKey: credentials.private_key,
          }),
        });
      })
      .catch((error) => {
        logger.error(
          'Error while getting firebase service account credentials',
          { error }
        );
        admin.initializeApp();
      });
  }

  public async validateToken(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const includePath = [
        '/group',
        '/trip',
        '/pickup-dropoff',
        '/child',
        '/driver',
        '/school',
      ];
      let validateToken = false;

      for (const path of includePath) {
        if (req.path.startsWith(path)) {
          validateToken = true;
          break;
        }
      }

      if (validateToken) {
        let token = null;
        if (
          req.headers.authorization &&
          req.headers.authorization.split(' ')[0] === 'Bearer'
        ) {
          token = req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
          token = req.query.token.toString();
        }

        if (!token) {
          throw new Error('Authentication token not supplied');
        }

        logger.info(`Getting decodedToken for token: ${token}`);
        const decodedToken = await admin.auth().verifyIdToken(token);
        logger.info('Done getting decodedToken', { token, decodedToken });
        if (
          moment().utc().isBefore(moment.unix(decodedToken.exp)) &&
          moment().utc().isAfter(moment.unix(decodedToken.iat)) &&
          moment.utc().isAfter(moment.unix(decodedToken.auth_time))
        ) {
          const requestWithBody = req as RequestWithToken &
            RequestWithPhoneNumber;
          requestWithBody.validatedToken = decodedToken;
          requestWithBody.validatedPhoneNumber =
            decodedToken.firebase?.identities?.phone?.[0];
          next();
        } else {
          throw new Error('Authentication token not valid');
        }
      } else {
        next();
      }
    } catch (error) {
      this._error.throwFormattedError({
        status: 401,
        message: 'Error while decoding token',
        source: 'int',
        errorData: {
          error,
        },
      });
    }
  }
}
