import { inject, injectable } from "inversify";

import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { UserType } from "../models/user-type";
import { UserTypeDataAccessInterface } from "./user-type.interface";

@injectable()
export class UserTypeDataAccess implements UserTypeDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<UserTypeDataAccess>());
  }

  async create(userType: UserType): Promise<UserType | null> {
    try {
      if (userType.save) {
        return await userType.save();
      }
      return await UserType.create(userType);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while creating userType",
        source: "intdb",
        errorData: {
          error,
          userType,
        },
      });
    }
  }
}
