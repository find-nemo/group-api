import { inject, injectable } from 'inversify';
import moment from 'moment';
import { Op } from 'sequelize';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Device } from '../models/device';
import { Group } from '../models/group';
import { Trip } from '../models/trip';
import { User } from '../models/user';
import { TripDataAccessInterface } from './trip.interface';

@injectable()
export class TripDataAccess implements TripDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<TripDataAccess>());
  }

  async getByGroupId(groupId: number): Promise<Trip[] | null> {
    try {
      return await Trip.findAll({
        where: {
          groupId,
        },
        include: [
          { model: Group, required: true },
          { model: Device, required: true },
          { model: User, required: true },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting trip by groupId',
        source: 'intdb',
        errorData: {
          error,
          groupId,
        },
      });
    }
  }

  async getByTripId(tripId: number): Promise<Trip | null> {
    try {
      return await Trip.findOne({
        where: {
          id: tripId,
        },
        include: [{ model: User, required: true }],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting trip by tripId',
        source: 'intdb',
        errorData: {
          error,
          tripId,
        },
      });
    }
  }

  async start(trip: Trip): Promise<Trip> {
    try {
      if (trip.save) {
        return await trip.save();
      }
      return await Trip.create(trip);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating trip for groupId',
        source: 'intdb',
        errorData: {
          error,
          trip,
        },
      });
    }
  }

  async end(
    tripId: number,
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]> {
    try {
      return await Trip.update(
        { endTime: moment.utc().toDate(), endLatitude, endLongitude },
        { where: { id: tripId } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while ending trip for groupId',
        source: 'intdb',
        errorData: {
          error,
          tripId,
        },
      });
    }
  }

  async getLatestTripByGroupIds(groupIds: number[]): Promise<Trip | null> {
    try {
      return await Trip.findOne({
        where: { groupId: { [Op.in]: groupIds } },
        include: [
          {
            model: Group,
            required: true,
          },
          { model: Device, required: true },
          { model: User, required: true },
        ],
        order: [['start_time', 'DESC']],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting latest trip for groupIds',
        source: 'intdb',
        errorData: {
          error,
          groupIds,
        },
      });
    }
  }

  async getLatestTripByGroupId(groupId: number): Promise<Trip | null> {
    try {
      return await Trip.findOne({
        where: { groupId },
        order: [['start_time', 'DESC']],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting latest trip for groupId',
        source: 'intdb',
        errorData: {
          error,
          groupId,
        },
      });
    }
  }

  async getActiveTrips(): Promise<Trip[]> {
    try {
      let offset = 0;
      const limit = 500;
      let trips: Trip[] = [];

      // eslint-disable-next-line no-constant-condition
      while (true) {
        const data = await Trip.findAll({
          limit,
          offset,
          where: {
            endTime: { [Op.is]: null },
          },
        });

        if (data && data.length) {
          trips = [...trips, ...data];
          if (data.length == limit) {
            offset += limit;
          } else {
            break;
          }
        } else {
          break;
        }
      }
      return trips;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting active trips',
        source: 'intdb',
        errorData: {
          error,
        },
      });
    }
  }

  async updateTripDriverLocation(
    tripId: number,
    latitude?: number,
    longitude?: number
  ): Promise<[number]> {
    try {
      return await Trip.update(
        {
          driverCurrentLatitude: latitude,
          driverCurrentLongitude: longitude,
        },
        {
          where: {
            id: tripId,
          },
        }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating trip driver current location',
        source: 'intdb',
        errorData: {
          error,
        },
      });
    }
  }

  async endInactiveTrips(): Promise<[number]> {
    try {
      return await Trip.update(
        {
          endTime: moment.utc().toDate(),
        },
        {
          where: {
            updatedAt: { [Op.lte]: moment().subtract(2, 'hours').toDate() },
          },
        }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while ending inactive trips',
        source: 'intdb',
        errorData: {
          error,
        },
      });
    }
  }

  async getActiveTripById(tripIds: number[]): Promise<Trip[]> {
    try {
      return await Trip.findAll({
        where: {
          id: { [Op.in]: tripIds },
          endTime: { [Op.is]: null},
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting active trips by id',
        source: 'intdb',
        errorData: {
          error,
          tripIds,
        },
      });
    }
  }
}
