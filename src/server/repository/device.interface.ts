import { Device } from '../models/device';

export interface DeviceDataAccessInterface {
  getByUserIds(userIds: number[]): Promise<Device[] | null>;
  getByPhoneNumberAndUId(
    phoneNumber: string,
    uId: string
  ): Promise<Device | null>;
}
