import { Child } from '../models/child';

export interface ChildDataAccessInterface {
  create(child: Child): Promise<Child>;
}
