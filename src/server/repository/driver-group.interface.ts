import { DriverGroup } from '../models/driver-group';

export interface DriverGroupDataAccessInterface {
  getByDriverId(driverId: number): Promise<DriverGroup[] | null>;
  getGroupIdByDriverId(driverId: number): Promise<DriverGroup[] | null>;
  create(driverGroup: DriverGroup): Promise<DriverGroup>;
  getByGroupId(
    groupId: number,
    includeUser?: boolean
  ): Promise<DriverGroup[] | null>;
  update(driverGroup: DriverGroup): Promise<[DriverGroup, boolean | null]>;
  getByDriverIdAndGroupId(
    driverId: number,
    groupId: number
  ): Promise<DriverGroup | null>;
  deleteDriverFromGroup(groupId: number, driverId: number): Promise<number>;
}
