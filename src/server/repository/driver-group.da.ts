import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { DriverGroup } from '../models/driver-group';
import { Group } from '../models/group';
import { User } from '../models/user';
import { DriverGroupDataAccessInterface } from './driver-group.interface';

@injectable()
export class DriverGroupDataAccess implements DriverGroupDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DriverGroupDataAccess>());
  }

  async getByDriverId(driverId: number): Promise<DriverGroup[] | null> {
    try {
      return await DriverGroup.findAll({
        where: {
          driverId,
        },
        include: [
          { model: Group, required: true },
          { model: User, required: true },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting driver-group by driverId',
        source: 'intdb',
        errorData: {
          error,
          driverId,
        },
      });
    }
  }

  async getGroupIdByDriverId(driverId: number): Promise<DriverGroup[] | null> {
    try {
      return await DriverGroup.findAll({
        where: {
          driverId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting driver-group by driverId',
        source: 'intdb',
        errorData: {
          error,
          driverId,
        },
      });
    }
  }

  async getByDriverIdAndGroupId(
    driverId: number,
    groupId: number
  ): Promise<DriverGroup | null> {
    try {
      return await DriverGroup.findOne({
        where: {
          driverId,
          groupId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting driver-group by driverId and groupId',
        source: 'intdb',
        errorData: {
          error,
          driverId,
          groupId,
        },
      });
    }
  }

  async create(driverGroup: DriverGroup): Promise<DriverGroup> {
    try {
      if (driverGroup.save) {
        return await driverGroup.save();
      }
      return await DriverGroup.create(driverGroup);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating driver-group',
        source: 'intdb',
        errorData: {
          error,
          driverGroup,
        },
      });
    }
  }

  async update(driverGroup: DriverGroup): Promise<[DriverGroup, boolean | null]> {
    try {
      return await DriverGroup.upsert(driverGroup);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while upsert driver-group',
        source: 'intdb',
        errorData: {
          error,
          driverGroup,
        },
      });
    }
  }

  async getByGroupId(
    groupId: number,
    includeUser = false
  ): Promise<DriverGroup[] | null> {
    try {
      return await DriverGroup.findAll({
        where: {
          groupId,
        },
        include: includeUser ? [{ model: User, required: true }] : [],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting driver-group by groupId',
        source: 'intdb',
        errorData: {
          error,
          groupId,
        },
      });
    }
  }

  async deleteDriverFromGroup(
    groupId: number,
    driverId: number
  ): Promise<number> {
    try {
      return await DriverGroup.destroy({
        where: {
          groupId,
          driverId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting driver-group',
        source: 'intdb',
        errorData: {
          error,
          groupId,
          driverId,
        },
      });
    }
  }
}
