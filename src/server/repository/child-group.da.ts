import { inject, injectable } from "inversify";
import { Op } from "sequelize";

import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { Address } from "../models/address";
import { Child } from "../models/child";
import { ChildGroup } from "../models/child-group";
import { Group } from "../models/group";
import { User } from "../models/user";
import { ChildGroupDataAccessInterface } from "./child-group.interface";
import { DriverGroupDataAccess } from "./driver-group.da";

@injectable()
export class ChildGroupDataAccess implements ChildGroupDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DriverGroupDataAccess>());
  }

  async getByChildId(childId: number): Promise<ChildGroup[] | null> {
    try {
      return await ChildGroup.findAll({
        where: { childId },
        include: [
          { model: Group, required: true },
          { model: Child, required: true },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting child-group by childId",
        source: "intdb",
        errorData: {
          error,
          childId,
        },
      });
    }
  }

  async getByChildIds(childIds: number[]): Promise<ChildGroup[] | null> {
    try {
      return await ChildGroup.findAll({
        where: { childId: { [Op.in]: childIds } },
        include: [
          { model: Group, required: true },
          {
            model: Child,
            required: true,
            include: [{ model: User, required: true }],
          },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting child-group by childIds",
        source: "intdb",
        errorData: {
          error,
          childIds,
        },
      });
    }
  }

  async getByGroupIds(groupIds: number[]): Promise<ChildGroup[]> {
    let offset = 0;
    const limit = 500;
    let childGroups: ChildGroup[] = [];
    try {
      // eslint-disable-next-line no-constant-condition
      while (true) {
        const data = await ChildGroup.findAll({
          limit,
          offset,
          where: { groupId: { [Op.in]: groupIds } },
          include: [
            {
              model: Child,
              required: true,
              include: [{ model: Address, required: true }],
            },
          ],
        });

        if (data && data.length) {
          childGroups = [...childGroups, ...data];
          if (data.length == limit) {
            offset += limit;
          } else {
            break;
          }
        } else {
          break;
        }
      }
      return childGroups;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting child-group by groupIds",
        source: "intdb",
        errorData: {
          error,
          groupIds,
        },
      });
    }
  }

  async getByUserId(userId: number): Promise<ChildGroup[] | null> {
    try {
      return await ChildGroup.findAll({
        include: [
          { model: Group, required: true },
          { model: Child, required: true, where: { parentId: userId } },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting child-group by userId",
        source: "intdb",
        errorData: {
          error,
          userId,
        },
      });
    }
  }

  async getByGroupId(
    groupId: number,
    includeUser = false
  ): Promise<ChildGroup[] | null> {
    try {
      return await ChildGroup.findAll({
        where: {
          groupId,
        },
        include: [
          { model: Group, required: true },
          {
            model: Child,
            required: true,
            include: includeUser ? [{ model: User, required: true }] : [],
          },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting child-group by groupId",
        source: "intdb",
        errorData: {
          error,
          groupId,
        },
      });
    }
  }

  async bulkCreate(childGroups: ChildGroup[]): Promise<ChildGroup[]> {
    try {
      return await ChildGroup.bulkCreate(childGroups, {
        ignoreDuplicates: true,
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while adding child-group",
        source: "intdb",
        errorData: {
          error,
          childGroups,
        },
      });
    }
  }

  async deleteByGroupIdAndChildIds(
    groupId: number,
    childIds: number[]
  ): Promise<number> {
    try {
      return await ChildGroup.destroy({
        where: {
          groupId,
          childId: { [Op.in]: childIds },
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while deleting child-group",
        source: "intdb",
        errorData: {
          error,
          childIds,
        },
      });
    }
  }

  async updateChildGroupMonthlyFees(childGroup: ChildGroup): Promise<[number]> {
    try {
      const updateValues: Partial<ChildGroup> = {};
      if (childGroup.monthFees) {
        updateValues.monthFees = childGroup.monthFees;
      }
      if (childGroup.threeMonthsFees) {
        updateValues.threeMonthsFees = childGroup.threeMonthsFees;
      }
      if (childGroup.sixMonthsFees) {
        updateValues.sixMonthsFees = childGroup.sixMonthsFees;
      }
      if (childGroup.twelveMonthsFees) {
        updateValues.twelveMonthsFees = childGroup.twelveMonthsFees;
      }
      return await ChildGroup.update(updateValues, {
        where: {
          groupId: childGroup.groupId,
          childId: childGroup.childId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while upadting child-group monthly fees",
        source: "intdb",
        errorData: {
          error,
          childGroup,
        },
      });
    }
  }

  async bulkUpdateChildGroupMonthlyFees(
    groupId: number,
    monthFees?: number,
    threeMonthsFees?: number,
    sixMonthsFees?: number,
    twelveMonthsFees?: number
  ): Promise<[number]> {
    try {
      const updateValues: Partial<ChildGroup> = {};
      if (monthFees) {
        updateValues.monthFees = monthFees;
      }
      if (threeMonthsFees) {
        updateValues.threeMonthsFees = threeMonthsFees;
      }
      if (sixMonthsFees) {
        updateValues.sixMonthsFees = sixMonthsFees;
      }
      if (twelveMonthsFees) {
        updateValues.twelveMonthsFees = twelveMonthsFees;
      }
      return await ChildGroup.update(updateValues, {
        where: {
          groupId: groupId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while upadting child-group fees",
        source: "intdb",
        errorData: {
          error,
          groupId,
          monthFees,
          threeMonthsFees,
          sixMonthsFees,
          twelveMonthsFees,
        },
      });
    }
  }
}
