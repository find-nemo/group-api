import { inject, injectable } from 'inversify';
import { Op } from 'sequelize';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Device } from '../models/device';
import { DeviceDataAccessInterface } from './device.interface';
import { User } from '../models/user';

@injectable()
export class DeviceDataAccess implements DeviceDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DeviceDataAccess>());
  }

  async getByUserIds(userIds: number[]): Promise<Device[] | null> {
    try {
      return await Device.findAll({
        where: {
          userId: { [Op.in]: userIds },
          fcmToken: { [Op.not]: null },
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting devices by userId and uId',
        source: 'intdb',
        errorData: {
          error,
          userIds,
        },
      });
    }
  }

  async getByPhoneNumberAndUId(
    phoneNumber: string,
    uId: string
  ): Promise<Device | null> {
    try {
      return await Device.findOne({
        where: {
          uId,
        },
        include: [
          {
            model: User,
            required: true,
            where: {
              phoneNumber,
            },
          },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting devices by userId and uId',
        source: 'intdb',
        errorData: {
          error,
          phoneNumber,
          uId,
        },
      });
    }
  }
}
