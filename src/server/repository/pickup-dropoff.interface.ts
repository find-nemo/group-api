import { PickupDropoff } from '../models/pickup-dropoff';

export interface PickupDropoffDataAccessInterface {
  getByTripId(tripId: number): Promise<PickupDropoff[] | null>;
  getByTripIds(tripIds: number[]): Promise<PickupDropoff[] | null>;
  pickupChilds(
    pickupDropoffs: Partial<PickupDropoff>[]
  ): Promise<PickupDropoff[]>;
  dropoffChilds(
    childIds: number[],
    tripId: number,
    latitude: number,
    longitude: number
  ): Promise<[number]>;
  dropoffAllChildsByTripId(
    tripId: number,
    pickupDropoffIds: number[],
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]>;
  getAllPickupChildsByTripId(tripId: number): Promise<PickupDropoff[]>;
  revertAbsentMarkedChildsByTridId(
    tripId: number,
    childIds: number[]
  ): Promise<number>;
}
