import { UserType } from "../models/user-type";

export interface UserTypeDataAccessInterface {
  create(userType: UserType): Promise<UserType | null>;
}
