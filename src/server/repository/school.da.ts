import { inject, injectable } from 'inversify';
import { Includeable } from 'sequelize/types';
import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Address } from '../models/address';
import { School } from '../models/school';
import { User } from '../models/user';
import { Op } from 'sequelize';
import { SchoolDataAccessInterface } from './school.interface';

@injectable()
export class SchoolDataAccess implements SchoolDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<SchoolDataAccess>());
  }

  async searchByName(
    userId: number,
    name: string,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]> {
    const _includeOptions: Includeable[] = [];
    if (includeCreator) {
      _includeOptions.push({ model: User, required: true });
    }
    if (includeAddress) {
      _includeOptions.push({ model: Address, required: true });
    }
    try {
      return await School.findAll({
        where: {
          fullName: { [Op.like]: `%${name}%` },
          [Op.or]: [
            {
              createdBy: null,
            },
            {
              createdBy: userId,
            },
          ],
        },
        limit: 20,
        include: _includeOptions,
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting school by name',
        source: 'intdb',
        errorData: {
          error,
          name,
        },
      });
    }
  }

  async searchByIds(
    ids: number[],
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]> {
    const _includeOptions: Includeable[] = [];
    if (includeCreator) {
      _includeOptions.push({ model: User, required: false });
    }
    if (includeAddress) {
      _includeOptions.push({ model: Address, required: true });
    }
    try {
      return await School.findAll({
        where: {
          id: { [Op.in]: ids },
        },
        include: _includeOptions,
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting school by ids',
        source: 'intdb',
        errorData: {
          error,
          ids,
        },
      });
    }
  }

  async searchById(
    id: number,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School | null> {
    const _includeOptions: Includeable[] = [];
    if (includeCreator) {
      _includeOptions.push({ model: User, required: true });
    }
    if (includeAddress) {
      _includeOptions.push({ model: Address, required: true });
    }
    try {
      return await School.findOne({
        where: {
          id: id,
        },
        include: _includeOptions,
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting school by id',
        source: 'intdb',
        errorData: {
          error,
          id,
        },
      });
    }
  }

  async create(school: School): Promise<School> {
    try {
      if (school.save) {
        return await school.save();
      }
      return await School.create(school);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating school',
        source: 'intdb',
        errorData: {
          error,
          school,
        },
      });
    }
  }
}
