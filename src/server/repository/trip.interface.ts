import { Trip } from '../models/trip';

export interface TripDataAccessInterface {
  getByGroupId(groupId: number): Promise<Trip[] | null>;
  start(trip: Trip): Promise<Trip>;
  end(
    tripId: number,
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]>;
  getLatestTripByGroupIds(groupIds: number[]): Promise<Trip | null>;
  getLatestTripByGroupId(groupId: number): Promise<Trip | null>;
  getByTripId(tripId: number): Promise<Trip | null>;
  getActiveTrips(): Promise<Trip[]>;
  updateTripDriverLocation(
    tripId: number,
    latitude?: number,
    longitude?: number
  ): Promise<[number]>;
  endInactiveTrips(): Promise<[number]>;
  getActiveTripById(tripIds: number[]): Promise<Trip[]>;
}
