import { injectable, inject } from "inversify";
import { UserDataAccessInterface } from "./user.interface";
import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { DriverGroupDataAccess } from "./driver-group.da";
import { Op } from "sequelize";
import { Child } from "../models/child";
import { User } from "../models/user";
import { UserType } from "../models/user-type";

@injectable()
export class UserDataAccess implements UserDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DriverGroupDataAccess>());
  }

  async getUserIdsByChildsIds(childIds: number[]): Promise<Child[]> {
    try {
      const data = await Child.findAll({
        where: {
          id: { [Op.in]: childIds },
        },
        attributes: ["parentId"],
      });
      return data;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while deleting child-group",
        source: "intdb",
        errorData: {
          error,
          childIds,
        },
      });
    }
  }

  async getByPhoneNumber(phoneNumber: string): Promise<User | null> {
    try {
      return await User.findOne({
        where: { phoneNumber },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async getChildsByPhoneNumber(phoneNumber: string): Promise<Child[] | null> {
    try {
      return await Child.findAll({
        include: [
          {
            model: User,
            where: {
              phoneNumber,
            },
          },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting childs by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async getUserByPhoneNumber(phoneNumber: string): Promise<User | null> {
    try {
      return await User.findOne({
        where: {
          phoneNumber,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async createUser(user: User): Promise<User> {
    try {
      if (user.save) {
        return await user.save();
      }
      return await User.create(user);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while creating a new user",
        source: "intdb",
        errorData: {
          error,
          user,
        },
      });
    }
  }

  async addDriverRole(userType: UserType): Promise<UserType> {
    try {
      if (userType.save) {
        return await userType.save();
      }
      return await UserType.create(userType);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting creating a new userType",
        source: "intdb",
        errorData: {
          error,
          userType,
        },
      });
    }
  }
}
