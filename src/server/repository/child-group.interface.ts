import { ChildGroup } from "../models/child-group";

export interface ChildGroupDataAccessInterface {
  getByUserId(userId: number): Promise<ChildGroup[] | null>;
  getByChildId(childId: number): Promise<ChildGroup[] | null>;
  getByGroupId(
    groupId: number,
    includeUser?: boolean
  ): Promise<ChildGroup[] | null>;
  bulkCreate(childGroups: Partial<ChildGroup>[]): Promise<ChildGroup[]>;
  deleteByGroupIdAndChildIds(
    groupId: number,
    childIds: number[]
  ): Promise<number>;
  getByChildIds(childIds: number[]): Promise<ChildGroup[] | null>;
  getByGroupIds(groupIds: number[]): Promise<ChildGroup[]>;
  updateChildGroupMonthlyFees(childGroup: ChildGroup): Promise<[number]>;
  bulkUpdateChildGroupMonthlyFees(
    groupId: number,
    monthFees?: number,
    threeMonthsFees?: number,
    sixMonthsFees?: number,
    twelveMonthsFees?: number
  ): Promise<[number]>;
}
