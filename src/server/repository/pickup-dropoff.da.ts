import { inject, injectable } from 'inversify';
import moment from 'moment';
import { Op } from 'sequelize';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Child } from '../models/child';
import { PickupDropoff } from '../models/pickup-dropoff';
import { Trip } from '../models/trip';
import { User } from '../models/user';
import { PickupDropoffDataAccessInterface } from './pickup-dropoff.interface';

@injectable()
export class PickupDropoffDataAccess
  implements PickupDropoffDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<PickupDropoffDataAccess>());
  }

  async getByTripId(tripId: number): Promise<PickupDropoff[] | null> {
    try {
      return await PickupDropoff.findAll({
        where: {
          tripId,
        },
        include: [
          { model: Trip, required: true },
          { model: Child, required: true },
          { model: User, required: true },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting pickup-dropoff by tripId',
        source: 'intdb',
        errorData: {
          error,
          tripId,
        },
      });
    }
  }
  async getByTripIds(tripIds: number[]): Promise<PickupDropoff[] | null> {
    try {
      let offset = 0;
      const limit = 500;
      let pickupDropoff: PickupDropoff[] = [];

      // eslint-disable-next-line no-constant-condition
      while (true) {
        const data = await PickupDropoff.findAll({
          limit,
          offset,
          where: {
            tripId: { [Op.in]: tripIds },
          },
        });
        if (data && data.length) {
          pickupDropoff = [...pickupDropoff, ...data];
          if (data.length == limit) {
            offset += limit;
          } else {
            break;
          }
        } else {
          break;
        }
      }
      return pickupDropoff;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting pickup-dropoff by tripIds',
        source: 'intdb',
        errorData: {
          error,
          tripIds,
        },
      });
    }
  }

  async pickupChilds(
    pickupDropoffs: PickupDropoff[]
  ): Promise<PickupDropoff[]> {
    try {
      return await PickupDropoff.bulkCreate(pickupDropoffs, {
        ignoreDuplicates: true,
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while pickup childs',
        source: 'intdb',
        errorData: {
          error,
          pickupDropoffs,
        },
      });
    }
  }

  async dropoffChilds(
    childIds: number[],
    tripId: number,
    latitude: number,
    longitude: number
  ): Promise<[number]> {
    try {
      return await PickupDropoff.update(
        {
          dropoffTime: moment.utc().toDate(),
          dropoffLatitude: latitude,
          dropoffLongitude: longitude,
        },
        {
          where: {
            tripId,
            childId: { [Op.in]: childIds },
            dropoffTime: { [Op.eq]: null },
          },
        }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while dropoff childs',
        source: 'intdb',
        errorData: {
          error,
          childIds,
          tripId,
        },
      });
    }
  }

  async dropoffAllChildsByTripId(
    tripId: number,
    pickupDropoffIds: number[],
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]> {
    try {
      return await PickupDropoff.update(
        {
          dropoffTime: moment.utc().toDate(),
          dropoffLatitude: endLatitude,
          dropoffLongitude: endLongitude,
        },
        { where: { tripId, id: { [Op.in]: pickupDropoffIds } } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while dropoff childs',
        source: 'intdb',
        errorData: {
          error,
          tripId,
        },
      });
    }
  }

  async getAllPickupChildsByTripId(tripId: number): Promise<PickupDropoff[]> {
    try {
      return await PickupDropoff.findAll({
        where: { tripId, dropoffTime: { [Op.eq]: null } },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting all pickup childs by tripId',
        source: 'intdb',
        errorData: {
          error,
          tripId,
        },
      });
    }
  }

  async revertAbsentMarkedChildsByTridId(
    tripId: number,
    childIds: number[]
  ): Promise<number> {
    try {
      return await PickupDropoff.destroy({
        where: {
          tripId,
          childId: { [Op.in]: childIds },
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while reverting absent childs by tripId',
        source: 'intdb',
        errorData: {
          error,
          tripId,
          childIds,
        },
      });
    }
  }
}
