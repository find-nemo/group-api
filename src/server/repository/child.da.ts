import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Child } from '../models/child';
import { ChildDataAccessInterface } from './child.interface';

@injectable()
export class ChildDataAccess implements ChildDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<ChildDataAccess>());
  }

  async create(child: Child): Promise<Child> {
    try {
      if (child.save) {
        return await child.save();
      }
      return await Child.create(child);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating child',
        source: 'intdb',
        errorData: {
          error,
          child,
        },
      });
    }
  }
}
