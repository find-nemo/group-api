import { inject, injectable } from 'inversify';
import { Op } from 'sequelize';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { DriverGroup } from '../models/driver-group';
import { Group } from '../models/group';
import { GroupDataAccessInterface } from './group.interface';

@injectable()
export class GroupDataAccess implements GroupDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<GroupDataAccess>());
  }

  async getByDriverId(driverId: number): Promise<Group[] | null> {
    try {
      const dgs = await DriverGroup.findAll({
        where: {
          driverId,
        }
      });
      if (dgs) {
        return await Group.findAll({
          where: {
            id: {[Op.in]: dgs.map(x => x.groupId)},
          },
        });
      }
      return null;
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting groups by driverId',
        source: 'intdb',
        errorData: {
          error,
          driverId,
        },
      });
    }
  }

  async getByPublicId(publicId: string): Promise<Group | null> {
    try {
      return await Group.findOne({
        where: { publicId },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting groups by publicId',
        source: 'intdb',
        errorData: {
          error,
          publicId,
        },
      });
    }
  }

  async getByGroupId(groupId: number): Promise<Group | null> {
    try {
      return await Group.findOne({
        where: {
          id: groupId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting groups by id',
        source: 'intdb',
        errorData: {
          error,
          groupId,
        },
      });
    }
  }

  async create(group: Group): Promise<Group> {
    try {
      if (group.save) {
        return await group.save();
      }
      return await Group.create(group);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating group',
        source: 'intdb',
        errorData: {
          error,
          group,
        },
      });
    }
  }

  async update(group: Group): Promise<[number]> {
    try {
      const updateValues: Partial<Group> = {};
      if (group.name) {
        updateValues.name = group.name;
      }
      if (group.schoolId) {
        updateValues.schoolId = group.schoolId;
      }
      if (group.toTime) {
        updateValues.toTime = group.toTime;
      }
      if (group.fromTime) {
        updateValues.fromTime = group.fromTime;
      }
      if (group.monthFees) {
        updateValues.monthFees = group.monthFees;
      }
      if (group.threeMonthsFees) {
        updateValues.threeMonthsFees = group.threeMonthsFees;
      }
      if (group.sixMonthsFees) {
        updateValues.sixMonthsFees = group.sixMonthsFees;
      }
      if (group.twelveMonthsFees) {
        updateValues.twelveMonthsFees = group.twelveMonthsFees;
      }
      return await Group.update(updateValues, { where: { id: group.id } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating group',
        source: 'intdb',
        errorData: {
          error,
          group,
        },
      });
    }
  }

  async deleteById(groupId: number): Promise<[number]> {
    try {
      return await Group.update(
        { isDeleted: true },
        { where: { id: groupId } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting group by id',
        source: 'intdb',
        errorData: {
          error,
          groupId,
        },
      });
    }
  }
}
