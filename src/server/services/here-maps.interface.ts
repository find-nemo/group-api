import { HereMapsFindSequenceResponse } from '../models/here-maps-find-sequence';

export interface HereMapsServiceInterface {
  findSequence(
    origin: string,
    destination: string,
    waypoints: string[]
  ): Promise<HereMapsFindSequenceResponse | null>;
}
