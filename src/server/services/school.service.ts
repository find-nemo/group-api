import { inject, injectable } from 'inversify';
import { School } from '../models/school';
import { SchoolDataAccessInterface } from '../repository/school.interface';
import { SchoolServiceInterface } from './school.interface';

@injectable()
export class SchoolService implements SchoolServiceInterface {
  constructor(
    @inject(nameof<SchoolDataAccessInterface>())
    private _schoolDataAccess: SchoolDataAccessInterface
  ) { }

  async searchByName(
    userId: number,
    name: string,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]> {
    return this._schoolDataAccess.searchByName(
      userId,
      name,
      includeCreator,
      includeAddress
    );
  }

  async searchById(
    id: number,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School | null> {
    return this._schoolDataAccess.searchById(
      id,
      includeCreator,
      includeAddress
    );
  }

  async searchByIds(
    ids: number[],
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]> {
    return this._schoolDataAccess.searchByIds(
      ids,
      includeCreator,
      includeAddress
    );
  }

  async create(school: School): Promise<School> {
    return this._schoolDataAccess.create(school);
  }
}
