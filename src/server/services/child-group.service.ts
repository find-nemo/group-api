import { inject, injectable } from "inversify";

import { ChildGroup } from "../models/child-group";
import { ChildGroupDataAccessInterface } from "../repository/child-group.interface";
import { ChildGroupServiceInterface } from "./child-group.interface";
import { GroupServiceInterface } from "./group.interface";

@injectable()
export class ChildGroupService implements ChildGroupServiceInterface {
  constructor(
    @inject(nameof<ChildGroupDataAccessInterface>())
    private _childGroupDataAccess: ChildGroupDataAccessInterface,
    @inject(nameof<GroupServiceInterface>())
    private _groupService: GroupServiceInterface
  ) {}

  async getByChildId(childId: number): Promise<ChildGroup[] | null> {
    return this._childGroupDataAccess.getByChildId(childId);
  }

  async getByGroupId(groupId: number): Promise<ChildGroup[] | null> {
    return this._childGroupDataAccess.getByGroupId(groupId);
  }

  async getByUserId(userId: number): Promise<ChildGroup[] | null> {
    return this._childGroupDataAccess.getByUserId(userId);
  }

  async bulkCreate(groupId: number, childIds: number[]): Promise<ChildGroup[]> {
    const group = await this._groupService.getByGroupId(groupId);
    if (childIds && childIds.length) {
      const childGroups: Partial<ChildGroup>[] = [];
      childIds.forEach((childId) => {
        const childGroup: Partial<ChildGroup> = {
          groupId,
          childId,
          monthFees: group?.monthFees,
          threeMonthsFees: group?.threeMonthsFees,
          sixMonthsFees: group?.sixMonthsFees,
          twelveMonthsFees: group?.twelveMonthsFees,
        };
        childGroups.push(childGroup);
      });
      return await this._childGroupDataAccess.bulkCreate(childGroups);
    } else {
      return [];
    }
  }

  async deleteByGroupIdAndChildIds(
    groupId: number,
    childIds: number[]
  ): Promise<number> {
    return this._childGroupDataAccess.deleteByGroupIdAndChildIds(
      groupId,
      childIds
    );
  }

  async getByGroupIds(groupIds: number[]): Promise<ChildGroup[]> {
    return this._childGroupDataAccess.getByGroupIds(groupIds);
  }

  async updateChildGroupMonthlyFees(childGroup: ChildGroup): Promise<[number]> {
    return this._childGroupDataAccess.updateChildGroupMonthlyFees(childGroup);
  }

  async bulkUpdateChildGroupMonthlyFees(
    groupId: number,
    monthFees?: number,
    threeMonthsFees?: number,
    sixMonthsFees?: number,
    twelveMonthsFees?: number
  ): Promise<[number]> {
    return this._childGroupDataAccess.bulkUpdateChildGroupMonthlyFees(
      groupId,
      monthFees,
      threeMonthsFees,
      sixMonthsFees,
      twelveMonthsFees
    );
  }
}
