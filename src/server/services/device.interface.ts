import { Device } from '../models/device';

export interface DeviceServiceInterface {
  getByPhoneNumberAndUId(
    phoneNumber: string,
    uId: string
  ): Promise<Device | null>;
}
