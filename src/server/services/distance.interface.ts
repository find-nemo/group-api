export interface DistanceServiceInterface {
  calculateActiveTripDuration(tripIds?: number[]): Promise<void>;
}
