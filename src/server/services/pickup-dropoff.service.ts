import { inject, injectable } from 'inversify';

import { PickupDropoff } from '../models/pickup-dropoff';
import { PickupDropoffDataAccessInterface } from '../repository/pickup-dropoff.interface';

@injectable()
export class PickupDropoffService implements PickupDropoffDataAccessInterface {
  constructor(
    @inject(nameof<PickupDropoffDataAccessInterface>())
    private _pickupDropoffDataAccess: PickupDropoffDataAccessInterface
  ) {}

  async getByTripId(tripId: number): Promise<PickupDropoff[] | null> {
    return this._pickupDropoffDataAccess.getByTripId(tripId);
  }

  async getByTripIds(tripIds: number[]): Promise<PickupDropoff[] | null> {
    return this._pickupDropoffDataAccess.getByTripIds(tripIds);
  }

  async pickupChilds(
    pickupDropoffs: Partial<PickupDropoff>[]
  ): Promise<PickupDropoff[]> {
    return this._pickupDropoffDataAccess.pickupChilds(pickupDropoffs);
  }

  async dropoffChilds(
    childIds: number[],
    tripId: number,
    latitude: number,
    longitude: number
  ): Promise<[number]> {
    return this._pickupDropoffDataAccess.dropoffChilds(
      childIds,
      tripId,
      latitude,
      longitude
    );
  }

  async dropoffAllChildsByTripId(
    tripId: number,
    pickupDropoffIds: number[],
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]> {
    return this._pickupDropoffDataAccess.dropoffAllChildsByTripId(
      tripId,
      pickupDropoffIds,
      endLatitude,
      endLongitude
    );
  }

  async getAllPickupChildsByTripId(tripId: number): Promise<PickupDropoff[]> {
    return this._pickupDropoffDataAccess.getAllPickupChildsByTripId(tripId);
  }

  async revertAbsentMarkedChildsByTridId(
    tripId: number,
    childIds: number[]
  ): Promise<number> {
    return this._pickupDropoffDataAccess.revertAbsentMarkedChildsByTridId(
      tripId,
      childIds
    );
  }
}
