import { Group } from '../models/group';

export interface GroupServiceInterface {
  getByPublicId(publicId: string): Promise<Group | null>;
  create(group: Group): Promise<Group>;
  update(group: Group): Promise<[number]>;
  deleteById(groupId: number): Promise<[number]>;
  getByGroupId(groupId: number): Promise<Group | null>;
}
