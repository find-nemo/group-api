import { Child } from '../models/child';

export interface ChildServiceInterface {
  create(child: Child): Promise<Child>;
}
