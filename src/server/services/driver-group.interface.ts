import { DriverGroup } from '../models/driver-group';
import { Trip } from '../models/trip';

export interface DriverGroupServiceInterface {
  getByDriverId(driverId: number): Promise<DriverGroup[] | null>;
  getLatestTripByDriverId(driverId: number): Promise<(Trip | null)[]>;
  findOrCreate(driverGroup: DriverGroup): Promise<DriverGroup>;
  deleteDriverFromGroup(groupId: number, driverId: number): Promise<number>;
  getByGroupId(
    groupId: number,
    includeUser?: boolean
  ): Promise<DriverGroup[] | null>;
}
