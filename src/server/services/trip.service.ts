import { inject, injectable } from 'inversify';

import { Trip } from '../models/trip';
import { TripDataAccessInterface } from '../repository/trip.interface';
import { TripServiceInterface } from './trip.interface';

@injectable()
export class TripService implements TripServiceInterface {
  constructor(
    @inject(nameof<TripDataAccessInterface>())
    private _tripDataAccess: TripDataAccessInterface
  ) {}
  async getByGroupId(groupId: number): Promise<Trip[] | null> {
    return this._tripDataAccess.getByGroupId(groupId);
  }

  async start(trip: Trip): Promise<Trip> {
    return this._tripDataAccess.start(trip);
  }

  async end(
    tripId: number,
    endLatitude: number,
    endLongitude: number
  ): Promise<[number]> {
    return this._tripDataAccess.end(tripId, endLatitude, endLongitude);
  }

  async getLatestTripByGroupIds(groupIds: number[]): Promise<Trip | null> {
    return this._tripDataAccess.getLatestTripByGroupIds(groupIds);
  }

  async getActiveTrips(): Promise<Trip[]> {
    return this._tripDataAccess.getActiveTrips();
  }

  async updateTripDriverLocation(
    tripId: number,
    latitude?: number,
    longitude?: number
  ): Promise<[number]> {
    return this._tripDataAccess.updateTripDriverLocation(
      tripId,
      latitude,
      longitude
    );
  }

  async endInactiveTrips(): Promise<[number]> {
    return this._tripDataAccess.endInactiveTrips();
  }

  async getActiveTripById(tripIds: number[]): Promise<Trip[]> {
    return this._tripDataAccess.getActiveTripById(tripIds);
  }
}
