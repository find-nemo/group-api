import { Child } from "../models/child";
import { User } from "../models/user";
import { UserType } from "../models/user-type";

export interface UserServiceInterface {
  getUserIdsByChildsIds(childIds: number[]): Promise<Child[]>;
  getByPhoneNumber(phoneNumber: string): Promise<User | null>;
  getChildsByPhoneNumber(phoneNumber: string): Promise<Child[] | null>;
  createUser(user: User): Promise<User>;
  addUserType(userType: UserType): Promise<UserType>;
  getUserByPhoneNumber(phoneNumber: string): Promise<User | null>;
}
