import { inject, injectable } from 'inversify';
import { UriOptions } from 'request';
import rp, { RequestPromiseOptions } from 'request-promise';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { HereMapsFindSequenceResponse } from '../models/here-maps-find-sequence';
import { HereMapsServiceInterface } from './here-maps.interface';

@injectable()
export class HereMapsService implements HereMapsServiceInterface {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<HereMapsService>());
  }
  async findSequence(
    origin: string,
    destination: string,
    waypoints: string[]
  ): Promise<HereMapsFindSequenceResponse | null> {
    const options: UriOptions & RequestPromiseOptions = {
      method: 'GET',
      uri: 'https://wse.ls.hereapi.com/2/findsequence.json',
      qs: {
        start: origin,
        end: destination,
        mode: 'fastest;car',
        apiKey: 'ADRN9N0jmBeb6l0QRP900A5SLgNDtHGMObYhQZJejco',
      },
      json: true,
    };

    if (waypoints && waypoints.length) {
      waypoints.forEach((w, i) => {
        options.qs[`destination${i + 1}`] = w;
      });
    }
    try {
      const data: HereMapsFindSequenceResponse = await rp(options);
      data.timestamp = Date.now();
      return data;
    } catch (error) {
      this._error.getFormattedError({
        source: 'extapi',
        status: 500,
        message: 'Error while getting sequence of the trip',
        errorData: {
          origin,
          destination,
          waypoints,
          reqOptions: options,
        },
      });
      return null;
    }
  }
}
