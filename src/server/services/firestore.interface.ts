import { FindSequenceByTrip } from '../models/find-sequence';

export interface FirestoreServiceInterface {
  updateGroupTimestampByUserIds(userIds: number[]): Promise<void>;
  updateTripDistance(sequences: (FindSequenceByTrip | null)[]): Promise<void>;
}
