export interface CloudMessagingInterface {
  startTripMessage(groupId: number, driverName: string): Promise<void>;
  endTripMessage(tripId: number): Promise<void>;
  pickupChildsMessage(childIds: number[], driverName: string): Promise<void>;
  dropoffChildsMessage(childIds: number[], driverName: string): Promise<void>;
  absentChildsMessage(childIds: number[], driverName: string): Promise<void>;
  notAbsentChildsMessage(childIds: number[], driverName: string): Promise<void>;
}
