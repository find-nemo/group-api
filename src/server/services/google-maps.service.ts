import { Client, DirectionsRequest, LatLng } from '@googlemaps/google-maps-services-js';
import { DirectionsResponseData } from '@googlemaps/google-maps-services-js/dist/directions';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { InterfaceSecretManagerPlugin } from '../../plugins/secret-manager.interface';
import { SecretKeys } from '../models/secrets';
import { GoogleMapsServiceInterface } from './google-maps.interface';

@injectable()
export class GoogleMapsService implements GoogleMapsServiceInterface {
  private _error: InterfaceErrorHandlerPlugin;
  private _client: Client;
  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<InterfaceSecretManagerPlugin>())
    private _secretManagerPlugin: InterfaceSecretManagerPlugin
  ) {
    this._error = errorFactory(nameof<GoogleMapsService>());
    this._client = new Client();
  }

  async findSequence(
    origin: LatLng,
    destination: LatLng,
    waypoints: LatLng[]
  ): Promise<DirectionsResponseData> {
    try {
      const key = await this._secretManagerPlugin.getSecretString(
        SecretKeys.directionapikey
      );
      if (!key) {
        throw this._error.getFormattedError({
          source: 'int',
          status: 500,
          message: 'Direction Api key not found',
        });
      }
      const options: DirectionsRequest = {
        params: {
          origin,
          destination,
          waypoints,
          optimize: true,
          key,
        },
      };
      return (await this._client.directions(options)).data;
    } catch (error) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 500,
        message: 'Error while getting sequence of the trip',
        errorData: {
          origin,
          destination,
          waypoints,
        },
      });
    }
  }
}
