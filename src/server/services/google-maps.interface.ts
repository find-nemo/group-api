import { LatLng } from '@googlemaps/google-maps-services-js';
import { DirectionsResponseData } from '@googlemaps/google-maps-services-js/dist/directions';

export interface GoogleMapsServiceInterface {
  findSequence(
    origin: LatLng,
    destination: LatLng,
    waypoints: LatLng[]
  ): Promise<DirectionsResponseData>;
}
