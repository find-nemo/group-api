import { inject, injectable } from 'inversify';

import { Child } from '../models/child';
import { ChildDataAccessInterface } from '../repository/child.interface';
import { ChildServiceInterface } from './child.interface';

@injectable()
export class ChildService implements ChildServiceInterface {
  constructor(
    @inject(nameof<ChildDataAccessInterface>())
    private _childDataAccess: ChildDataAccessInterface
  ) {}
  async create(child: Child): Promise<Child> {
    return this._childDataAccess.create(child);
  }
}
