import { ChildGroup } from "../models/child-group";

export interface ChildGroupServiceInterface {
  getByUserId(userId: number): Promise<ChildGroup[] | null>;
  getByChildId(childId: number): Promise<ChildGroup[] | null>;
  getByGroupId(groupId: number): Promise<ChildGroup[] | null>;
  bulkCreate(groupId: number, childIds: number[]): Promise<ChildGroup[]>;
  deleteByGroupIdAndChildIds(
    groupId: number,
    childIds: number[]
  ): Promise<number>;
  getByGroupIds(groupIds: number[]): Promise<ChildGroup[]>;
  updateChildGroupMonthlyFees(childGroup: ChildGroup): Promise<[number]>;
  bulkUpdateChildGroupMonthlyFees(
    groupId: number,
    monthFees?: number,
    threeMonthsFees?: number,
    sixMonthsFees?: number,
    twelveMonthsFees?: number
  ): Promise<[number]>;
}
