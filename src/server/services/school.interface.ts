import { School } from '../models/school';

export interface SchoolServiceInterface {
  searchByName(
    userId: number,
    name: string,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]>;
  searchById(
    id: number,
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School | null>;
  searchByIds(
    ids: number[],
    includeCreator: boolean,
    includeAddress: boolean
  ): Promise<School[]>;
  create(school: School): Promise<School>;
}
