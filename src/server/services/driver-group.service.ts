import { inject, injectable } from 'inversify';

import { DriverGroup } from '../models/driver-group';
import { Trip } from '../models/trip';
import { DriverGroupDataAccessInterface } from '../repository/driver-group.interface';
import { TripDataAccessInterface } from '../repository/trip.interface';
import { DriverGroupServiceInterface } from './driver-group.interface';

@injectable()
export class DriverGroupService implements DriverGroupServiceInterface {
  constructor(
    @inject(nameof<DriverGroupDataAccessInterface>())
    private _driverGroupDataAccess: DriverGroupDataAccessInterface,
    @inject(nameof<TripDataAccessInterface>())
    private _tripDataAccessInterface: TripDataAccessInterface
  ) {}

  async getByDriverId(driverId: number): Promise<DriverGroup[] | null> {
    const driverGroups = await this._driverGroupDataAccess.getByDriverId(
      driverId
    );
    if (driverGroups && driverGroups.length) {
      const promises: Promise<Trip | null>[] = [];
      driverGroups.forEach((dg) => {
        promises.push(
          this._tripDataAccessInterface.getLatestTripByGroupId(dg.groupId)
        );
      });

      const futures = await Promise.all(promises);

      futures.forEach((trip) => {
        if (trip) {
          driverGroups.forEach((dg) => {
            if (dg.groupId == trip.groupId) {
              dg.latestTrip = trip;
              dg.setDataValue('latestTrip', trip);
            }
          });
        }
      });
    }
    return driverGroups;
  }

  async getLatestTripByDriverId(driverId: number): Promise<(Trip | null)[]> {
    const driverGroups = await this._driverGroupDataAccess.getGroupIdByDriverId(
      driverId
    );
    if (driverGroups && driverGroups.length) {
      const promises: Promise<Trip | null>[] = [];
      driverGroups.forEach((dg) => {
        promises.push(
          this._tripDataAccessInterface.getLatestTripByGroupId(dg.groupId)
        );
      });

      return await Promise.all(promises);
    }
    return [];
  }

  async findOrCreate(driverGroup: DriverGroup): Promise<DriverGroup> {
    const _driverGroup = await this._driverGroupDataAccess.getByDriverIdAndGroupId(
      driverGroup.driverId,
      driverGroup.groupId
    );
    if (!_driverGroup) {
      return await this._driverGroupDataAccess.create(driverGroup);
    }
    return _driverGroup;
  }

  async deleteDriverFromGroup(
    groupId: number,
    driverId: number
  ): Promise<number> {
    return this._driverGroupDataAccess.deleteDriverFromGroup(groupId, driverId);
  }

  async getByGroupId(
    groupId: number,
    includeUser = false
  ): Promise<DriverGroup[] | null> {
    return this._driverGroupDataAccess.getByGroupId(groupId, includeUser);
  }
}
