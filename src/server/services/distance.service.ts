/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { LatLng } from '@googlemaps/google-maps-services-js';
import { inject, injectable } from 'inversify';

import { ChildGroup } from '../models/child-group';
import { FindSequenceByTrip, Interconnection } from '../models/find-sequence';
import { PickupDropoff } from '../models/pickup-dropoff';
import { Trip } from '../models/trip';
import { ChildGroupServiceInterface } from './child-group.interface';
import { DistanceServiceInterface } from './distance.interface';
import { FirestoreServiceInterface } from './firestore.interface';
import { GoogleMapsServiceInterface } from './google-maps.interface';
import { PickupDropoffServiceInterface } from './pickup-dropoff.interface';
import { TripServiceInterface } from './trip.interface';

@injectable()
export class DistanceService implements DistanceServiceInterface {
  constructor(
    @inject(nameof<TripServiceInterface>())
    private _tripService: TripServiceInterface,
    @inject(nameof<PickupDropoffServiceInterface>())
    private _pickupDropoffService: PickupDropoffServiceInterface,
    @inject(nameof<ChildGroupServiceInterface>())
    private _childGroupService: ChildGroupServiceInterface,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface,
    @inject(nameof<GoogleMapsServiceInterface>())
    private _googleMapsService: GoogleMapsServiceInterface
  ) {}

  async calculateActiveTripDuration(tripIds?: number[]): Promise<void> {
    let activeTrips: Trip[] = [];
    if (!tripIds) {
      activeTrips = await this._tripService.getActiveTrips();
    } else {
      activeTrips = await this._tripService.getActiveTripById(tripIds);
    }
    await this._calculateTripsDuration(activeTrips);
  }

  private async _calculateTripsDuration(activeTrips: Trip[]): Promise<void> {
    if (activeTrips && activeTrips.length) {
      const groupIds = activeTrips.map((x) => x.groupId);
      const promises = await Promise.all([
        this._pickupDropoffService.getByTripIds(activeTrips.map((x) => x.id)),
        this._childGroupService.getByGroupIds(groupIds),
      ]);
      const pickupDropoffs = promises[0];
      const childGroups = promises[1];

      const findSequencePromises: Promise<FindSequenceByTrip | null>[] = [];
      for (const trip of activeTrips) {
        const filterPickupDropoff = pickupDropoffs?.filter(
          (x) => x.tripId == trip.id
        );
        const filterChildGroups = childGroups.filter((x) =>
          this._shouldFilter(filterPickupDropoff, x, trip)
        );
        findSequencePromises.push(
          this._findSequenceByTrip(trip, filterChildGroups)
        );
      }
      await this._firestoreService.updateTripDistance(
        await Promise.all(findSequencePromises)
      );
    }
  }

  private _shouldFilter(
    pickupDropoffs: PickupDropoff[] | undefined,
    childGroup: ChildGroup,
    trip: Trip
  ): boolean {
    if (childGroup.groupId != trip.groupId) {
      return false;
    }
    if (pickupDropoffs == null || !pickupDropoffs.length) {
      return true;
    }
    const pickupDropoff = pickupDropoffs.filter(
      (x) => x.childId == childGroup.childId
    );
    if (pickupDropoff.length) {
      if (pickupDropoff[0].isAbsent) {
        return false;
      }
      if (
        (trip.type == 'TOWARDS' && pickupDropoff[0].pickupTime != null) ||
        pickupDropoff[0].dropoffTime != null
      ) {
        return false;
      }
    }
    return true;
  }

  private async _findSequenceByTrip(
    trip: Trip,
    childGroups: ChildGroup[]
  ): Promise<FindSequenceByTrip | null> {
    try {
      const origin = this._generateHereMapsLatLng(
        trip.driverCurrentLatitude! || trip.startLatitude!,
        trip.driverCurrentLongitude! || trip.startLongitude!
      );

      const destination = this._generateHereMapsLatLng(
        trip.startLatitude!,
        trip.startLongitude!
      );

      const wp = childGroups.map((x) =>
        this._generateHereMapsLatLng(
          x.child.address?.latitude!,
          x.child.address?.longitude!
        )
      );

      const data = await this._googleMapsService.findSequence(
        origin,
        destination,
        wp
      );

      const interconnections: Interconnection[] = [];
      const waypoints: string[] = [];

      data.routes[0].legs.forEach((leg, index) => {
        if (data.routes[0].legs.length == 1) {
          waypoints.push('origin');
          waypoints.push('destination');
          interconnections.push({
            fromWaypoint: 'origin',
            toWaypoint: `destination`,
            time: leg.duration.value,
            distance: leg.distance.value,
          });
        } else if (index == 0) {
          waypoints.push('origin');
          const key = `child_${childGroups[
            data.routes[0].waypoint_order[index]
          ].getDataValue('childId')}`;
          interconnections.push({
            fromWaypoint: 'origin',
            toWaypoint: key,
            time: leg.duration.value,
            distance: leg.distance.value,
          });
          if (data.routes[0].legs.length == 2) {
            waypoints.push(key);
          }
        } else if (index == data.routes[0].legs.length - 1) {
          const fromKey = `child_${childGroups[
            data.routes[0].waypoint_order[index - 1]
          ].getDataValue('childId')}`;
          interconnections.push({
            toWaypoint: 'destination',
            fromWaypoint: fromKey,
            time: leg.duration.value,
            distance: leg.distance.value,
          });
          waypoints.push(fromKey);
          waypoints.push('destination');
        } else {
          const toKey = `child_${childGroups[
            data.routes[0].waypoint_order[index]
          ].getDataValue('childId')}`;
          const fromKey = `child_${childGroups[
            data.routes[0].waypoint_order[index - 1]
          ].getDataValue('childId')}`;
          waypoints.push(fromKey);
          interconnections.push({
            toWaypoint: toKey,
            fromWaypoint: fromKey,
            time: leg.duration.value,
            distance: leg.distance.value,
          });
        }
      });

      return {
        tripId: trip.id,
        sequence: {
          interconnections,
          timestamp: Date.now(),
          waypoints,
        },
      };
    } catch (error) {
      return null;
    }
  }

  private _generateHereMapsLatLng(latitude: number, longitude: number): LatLng {
    return {
      latitude,
      longitude,
    };
  }
}
