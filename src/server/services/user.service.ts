import { injectable, inject } from "inversify";
import { UserServiceInterface } from "./user.interface";
import { Child } from "../models/child";
import { UserDataAccessInterface } from "../repository/user.interface";
import { User } from "../models/user";
import { UserType } from "../models/user-type";
import { UserTypeEnum } from "../models/user-type.enum";
import { UserTypeDataAccessInterface } from "../repository/user-type.interface";

@injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface,
    @inject(nameof<UserTypeDataAccessInterface>())
    private _userTypeDataAccess: UserTypeDataAccessInterface
  ) {}

  async getUserIdsByChildsIds(childIds: number[]): Promise<Child[]> {
    return this._userDataAccess.getUserIdsByChildsIds(childIds);
  }

  async getByPhoneNumber(phoneNumber: string): Promise<User | null> {
    return this._userDataAccess.getByPhoneNumber(phoneNumber);
  }

  async getChildsByPhoneNumber(phoneNumber: string): Promise<Child[] | null> {
    return this._userDataAccess.getChildsByPhoneNumber(phoneNumber);
  }

  async createUser(user: User): Promise<User> {
    const createdUser = await this._userDataAccess.createUser(user);

    // Create userType and set the user to parent
    const userType = new UserType();
    userType.isDriver =
      user.role === UserTypeEnum.DRIVER || user.role === UserTypeEnum.BOTH;
    userType.isParent =
      user.role === UserTypeEnum.PARENT || user.role === UserTypeEnum.BOTH;
    userType.userId = createdUser.id;
    await this._userTypeDataAccess.create(userType);

    return createdUser;
  }

  async addUserType(userType: UserType): Promise<UserType> {
    return this._userDataAccess.addDriverRole(userType);
  }

  async getUserByPhoneNumber(phoneNumber: string): Promise<User | null> {
    return this._userDataAccess.getUserByPhoneNumber(phoneNumber);
  }
}
