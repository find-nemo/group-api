import * as admin from 'firebase-admin';
import { injectable } from 'inversify';
import moment from 'moment';

import { config } from '../../config';
import { FindSequenceByTrip } from '../models/find-sequence';
import { FirestoreServiceInterface } from './firestore.interface';

@injectable()
export class FirestoreService implements FirestoreServiceInterface {
  async updateGroupTimestampByUserIds(userIds: number[]): Promise<void> {
    if (userIds && userIds.length) {
      const promises: Promise<FirebaseFirestore.WriteResult>[] = [];
      userIds.forEach((userId) => {
        promises.push(
          admin
            .firestore()
            .collection(config.googleCloudConfig.firestoreCollection)
            .doc(`userId_${userId}`)
            .set({ group: moment.utc().valueOf() }, { merge: true })
        );
      });
      await Promise.all(promises);
    }
  }

  async updateTripDistance(
    sequences: (FindSequenceByTrip | null)[]
  ): Promise<void> {
    if (sequences && sequences.length) {
      const promises: Promise<FirebaseFirestore.WriteResult>[] = [];
      sequences.forEach((s) => {
        if (s) {
          promises.push(
            admin
              .firestore()
              .collection(config.googleCloudConfig.firestoreCollection)
              .doc(`tripId_${s.tripId}`)
              .set(s.sequence, { merge: true })
          );
        }
      });
      await Promise.all(promises);
    }
  }
}
