import * as admin from 'firebase-admin';
import { inject, injectable } from 'inversify';
import _ from 'lodash';
import moment from 'moment';

import { config } from '../../config';
import { ChildGroupDataAccessInterface } from '../repository/child-group.interface';
import { DeviceDataAccessInterface } from '../repository/device.interface';
import { DriverGroupDataAccessInterface } from '../repository/driver-group.interface';
import { TripDataAccessInterface } from '../repository/trip.interface';
import { logger } from '../utils/logger';
import { CloudMessagingInterface } from './cloud-messaging.interface';

@injectable()
export class CloudMessagingService implements CloudMessagingInterface {
  constructor(
    @inject(nameof<ChildGroupDataAccessInterface>())
    private _childGroupDataAccess: ChildGroupDataAccessInterface,
    @inject(nameof<TripDataAccessInterface>())
    private _tripDataAccess: TripDataAccessInterface,
    @inject(nameof<DeviceDataAccessInterface>())
    private _deviceDataAccess: DeviceDataAccessInterface,
    @inject(nameof<DriverGroupDataAccessInterface>())
    private _driverGroupDataAccess: DriverGroupDataAccessInterface
  ) {}

  async startTripMessage(groupId: number, driverName: string): Promise<void> {
    try {
      const futures = await Promise.all([
        this._childGroupDataAccess.getByGroupId(groupId, true),
        this._driverGroupDataAccess.getByGroupId(groupId, false),
      ]);

      const childGroups = futures[0];
      const driverGroups = futures[1];

      let groupName: string | null = null;

      if (childGroups && childGroups.length) {
        const userIds = new Set<number>();
        childGroups.map((cg) => {
          if (!groupName && cg.group && cg.group.name) {
            groupName = cg.group.name;
          }
          if (cg.child && cg.child.parentId) {
            userIds.add(cg.child.parentId);
          }
        });

        if (userIds && userIds.size) {
          const devices = await this._deviceDataAccess.getByUserIds(
            Array.from(userIds)
          );

          const message: admin.messaging.MulticastMessage = {
            notification: {
              title: `${driverName} started trip for ${groupName} group`,
              body: `View ${driverName} location`,
            },
            tokens: [],
          };

          const promises = [];
          if (driverGroups && driverGroups.length) {
            driverGroups.forEach((d) => {
              promises.push(
                admin
                  .firestore()
                  .collection(config.googleCloudConfig.firestoreCollection)
                  .doc(`userId_${d.driverId}`)
                  .set({ trip: moment.utc().valueOf() }, { merge: true })
              );
            });
          }
          if (devices && devices.length) {
            for (const d of _.chunk(devices, 500)) {
              message.tokens = [];
              if (d && d.length) {
                d.forEach((device) => {
                  promises.push(
                    admin
                      .firestore()
                      .collection(config.googleCloudConfig.firestoreCollection)
                      .doc(`userId_${device.userId}`)
                      .set({ trip: moment.utc().valueOf() }, { merge: true })
                  );
                  const fcmToken = device.getDataValue('fcmToken');
                  if (fcmToken) {
                    message.tokens.push(fcmToken);
                  }
                });
              }
              if (message && message.tokens && message.tokens.length) {
                promises.push(admin.messaging().sendMulticast(message));
              }
            }
          }
          await Promise.all(promises);
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the parent devices',
        { groupId, driverName, error }
      );
    }
  }

  async endTripMessage(tripId: number): Promise<void> {
    try {
      const trip = await this._tripDataAccess.getByTripId(tripId);
      if (trip && trip.groupId && trip.driver && trip.driver.fullName) {
        const futures = await Promise.all([
          this._childGroupDataAccess.getByGroupId(trip.groupId, true),
          this._driverGroupDataAccess.getByGroupId(trip.groupId, false),
        ]);

        const childGroups = futures[0];
        const driverGroups = futures[1];

        let groupName: string | null = null;

        if (childGroups && childGroups.length) {
          const userIds = new Set<number>();
          childGroups.map((cg) => {
            if (!groupName && cg.group && cg.group.name) {
              groupName = cg.group.name;
            }
            if (cg.child && cg.child.parentId) {
              userIds.add(cg.child.parentId);
            }
          });

          if (userIds && userIds.size) {
            const devices = await this._deviceDataAccess.getByUserIds(
              Array.from(userIds)
            );

            const message: admin.messaging.MulticastMessage = {
              notification: {
                title: `${trip.driver.fullName} ended trip for ${groupName} group`,
                body: `View your child last drop-off location`,
              },
              tokens: [],
            };
            const promises = [];

            if (driverGroups && driverGroups.length) {
              driverGroups.forEach((d) => {
                promises.push(
                  admin
                    .firestore()
                    .collection(config.googleCloudConfig.firestoreCollection)
                    .doc(`userId_${d.driverId}`)
                    .set({ trip: moment.utc().valueOf() }, { merge: true })
                );
              });
            }
            if (devices && devices.length) {
              for (const d of _.chunk(devices, 500)) {
                message.tokens = [];
                if (d && d.length) {
                  d.forEach((device) => {
                    promises.push(
                      admin
                        .firestore()
                        .collection(
                          config.googleCloudConfig.firestoreCollection
                        )
                        .doc(`userId_${device.userId}`)
                        .set({ trip: moment.utc().valueOf() }, { merge: true })
                    );
                    const fcmToken = device.getDataValue('fcmToken');
                    if (fcmToken) {
                      message.tokens.push(fcmToken);
                    }
                  });
                }
                if (message && message.tokens && message.tokens.length) {
                  promises.push(admin.messaging().sendMulticast(message));
                }
              }
            }
            await Promise.all(promises);
          }
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the parent devices',
        { tripId, error }
      );
    }
  }

  async pickupChildsMessage(
    childIds: number[],
    driverName: string
  ): Promise<void> {
    try {
      const childGroups = await this._childGroupDataAccess.getByChildIds(
        childIds
      );
      let groupName: string | null = null;
      let groupId: number | null = null;

      if (childGroups && childGroups.length) {
        const userIds = new Set<number>();
        childGroups.map((cg) => {
          if (!groupName && cg.group && cg.group.name) {
            groupName = cg.group.name;
            groupId = cg.groupId;
          }
          if (cg.child && cg.child.parentId) {
            userIds.add(cg.child.parentId);
          }
        });

        if (userIds && userIds.size) {
          const devices = await this._deviceDataAccess.getByUserIds(
            Array.from(userIds)
          );

          const message: admin.messaging.MulticastMessage = {
            notification: {
              title: `${driverName} pick-up your child for ${groupName} group`,
              body: `View ${driverName} location`,
            },
            tokens: [],
          };

          const promises = [];
          if (groupId) {
            const driverGroups = await this._driverGroupDataAccess.getByGroupId(
              groupId,
              false
            );
            if (driverGroups && driverGroups.length) {
              driverGroups.forEach((d) => {
                promises.push(
                  admin
                    .firestore()
                    .collection(config.googleCloudConfig.firestoreCollection)
                    .doc(`userId_${d.driverId}`)
                    .set(
                      { pickupDropoff: moment.utc().valueOf() },
                      { merge: true }
                    )
                );
              });
            }
          }
          if (devices && devices.length) {
            for (const d of _.chunk(devices, 500)) {
              message.tokens = [];
              if (d && d.length) {
                d.forEach((device) => {
                  promises.push(
                    admin
                      .firestore()
                      .collection(config.googleCloudConfig.firestoreCollection)
                      .doc(`userId_${device.userId}`)
                      .set(
                        { pickupDropoff: moment.utc().valueOf() },
                        { merge: true }
                      )
                  );
                  const fcmToken = device.getDataValue('fcmToken');
                  if (fcmToken) {
                    message.tokens.push(fcmToken);
                  }
                });
              }
              if (message && message.tokens && message.tokens.length) {
                promises.push(admin.messaging().sendMulticast(message));
              }
            }
          }
          await Promise.all(promises);
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the child that are pickedup by the driver',
        { childIds, driverName, error }
      );
    }
  }

  async absentChildsMessage(
    childIds: number[],
    driverName: string
  ): Promise<void> {
    try {
      const childGroups = await this._childGroupDataAccess.getByChildIds(
        childIds
      );
      let groupName: string | null = null;
      let groupId: number | null = null;

      if (childGroups && childGroups.length) {
        const userIds = new Set<number>();
        childGroups.map((cg) => {
          if (!groupName && cg.group && cg.group.name) {
            groupName = cg.group.name;
            groupId = cg.groupId;
          }
          if (cg.child && cg.child.parentId) {
            userIds.add(cg.child.parentId);
          }
        });

        if (userIds && userIds.size) {
          const devices = await this._deviceDataAccess.getByUserIds(
            Array.from(userIds)
          );

          const message: admin.messaging.MulticastMessage = {
            notification: {
              title: `${driverName} marked your child absent for ${groupName} group`,
            },
            tokens: [],
          };

          const promises = [];
          if (groupId) {
            const driverGroups = await this._driverGroupDataAccess.getByGroupId(
              groupId,
              false
            );
            if (driverGroups && driverGroups.length) {
              driverGroups.forEach((d) => {
                promises.push(
                  admin
                    .firestore()
                    .collection(config.googleCloudConfig.firestoreCollection)
                    .doc(`userId_${d.driverId}`)
                    .set(
                      { pickupDropoff: moment.utc().valueOf() },
                      { merge: true }
                    )
                );
              });
            }
          }
          if (devices && devices.length) {
            for (const d of _.chunk(devices, 500)) {
              message.tokens = [];
              if (d && d.length) {
                d.forEach((device) => {
                  promises.push(
                    admin
                      .firestore()
                      .collection(config.googleCloudConfig.firestoreCollection)
                      .doc(`userId_${device.userId}`)
                      .set(
                        { pickupDropoff: moment.utc().valueOf() },
                        { merge: true }
                      )
                  );
                  const fcmToken = device.getDataValue('fcmToken');
                  if (fcmToken) {
                    message.tokens.push(fcmToken);
                  }
                });
              }
              if (message && message.tokens && message.tokens.length) {
                promises.push(admin.messaging().sendMulticast(message));
              }
            }
          }
          await Promise.all(promises);
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the child that are pickedup by the driver',
        { childIds, driverName, error }
      );
    }
  }

  async notAbsentChildsMessage(
    childIds: number[],
    driverName: string
  ): Promise<void> {
    try {
      const childGroups = await this._childGroupDataAccess.getByChildIds(
        childIds
      );
      let groupName: string | null = null;
      let groupId: number | null = null;

      if (childGroups && childGroups.length) {
        const userIds = new Set<number>();
        childGroups.map((cg) => {
          if (!groupName && cg.group && cg.group.name) {
            groupName = cg.group.name;
            groupId = cg.groupId;
          }
          if (cg.child && cg.child.parentId) {
            userIds.add(cg.child.parentId);
          }
        });

        if (userIds && userIds.size) {
          const devices = await this._deviceDataAccess.getByUserIds(
            Array.from(userIds)
          );

          const message: admin.messaging.MulticastMessage = {
            notification: {
              title: `${driverName} marked your child not absent for ${groupName} group`,
            },
            tokens: [],
          };

          const promises = [];
          if (groupId) {
            const driverGroups = await this._driverGroupDataAccess.getByGroupId(
              groupId,
              false
            );
            if (driverGroups && driverGroups.length) {
              driverGroups.forEach((d) => {
                promises.push(
                  admin
                    .firestore()
                    .collection(config.googleCloudConfig.firestoreCollection)
                    .doc(`userId_${d.driverId}`)
                    .set(
                      { pickupDropoff: moment.utc().valueOf() },
                      { merge: true }
                    )
                );
              });
            }
          }
          if (devices && devices.length) {
            for (const d of _.chunk(devices, 500)) {
              message.tokens = [];
              if (d && d.length) {
                d.forEach((device) => {
                  promises.push(
                    admin
                      .firestore()
                      .collection(config.googleCloudConfig.firestoreCollection)
                      .doc(`userId_${device.userId}`)
                      .set(
                        { pickupDropoff: moment.utc().valueOf() },
                        { merge: true }
                      )
                  );
                  const fcmToken = device.getDataValue('fcmToken');
                  if (fcmToken) {
                    message.tokens.push(fcmToken);
                  }
                });
              }
              if (message && message.tokens && message.tokens.length) {
                promises.push(admin.messaging().sendMulticast(message));
              }
            }
          }
          await Promise.all(promises);
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the child that are pickedup by the driver',
        { childIds, driverName, error }
      );
    }
  }

  async dropoffChildsMessage(
    childIds: number[],
    driverName: string
  ): Promise<void> {
    try {
      const childGroups = await this._childGroupDataAccess.getByChildIds(
        childIds
      );
      let groupName: string | null = null;
      let groupId: number | null = null;

      if (childGroups && childGroups.length) {
        const userIds = new Set<number>();
        childGroups.map((cg) => {
          if (!groupName && cg.group && cg.group.name) {
            groupName = cg.group.name;
            groupId = cg.groupId;
          }
          if (cg.child && cg.child.parentId) {
            userIds.add(cg.child.parentId);
          }
        });

        if (userIds && userIds.size) {
          const devices = await this._deviceDataAccess.getByUserIds(
            Array.from(userIds)
          );

          const message: admin.messaging.MulticastMessage = {
            notification: {
              title: `${driverName} drop-off your child for ${groupName} group`,
              body: `View your child last drop-off location`,
            },
            tokens: [],
          };

          const promises = [];
          if (groupId) {
            const driverGroups = await this._driverGroupDataAccess.getByGroupId(
              groupId,
              false
            );
            if (driverGroups && driverGroups.length) {
              driverGroups.forEach((d) => {
                promises.push(
                  admin
                    .firestore()
                    .collection(config.googleCloudConfig.firestoreCollection)
                    .doc(`userId_${d.driverId}`)
                    .set(
                      { pickupDropoff: moment.utc().valueOf() },
                      { merge: true }
                    )
                );
              });
            }
          }
          if (devices && devices.length) {
            for (const d of _.chunk(devices, 500)) {
              message.tokens = [];
              if (d && d.length) {
                d.forEach((device) => {
                  promises.push(
                    admin
                      .firestore()
                      .collection(config.googleCloudConfig.firestoreCollection)
                      .doc(`userId_${device.userId}`)
                      .set(
                        { pickupDropoff: moment.utc().valueOf() },
                        { merge: true }
                      )
                  );
                  const fcmToken = device.getDataValue('fcmToken');
                  if (fcmToken) {
                    message.tokens.push(fcmToken);
                  }
                });
              }
              if (message && message.tokens && message.tokens.length) {
                promises.push(admin.messaging().sendMulticast(message));
              }
            }
          }
          await Promise.all(promises);
        }
      }
    } catch (error) {
      logger.error(
        'Error while sending push notification to all the child that are dropoff by the driver',
        { childIds, driverName, error }
      );
    }
  }
}
