import { inject, injectable } from 'inversify';

import { Device } from '../models/device';
import { DeviceServiceInterface } from './device.interface';
import { DeviceDataAccessInterface } from '../repository/device.interface';

@injectable()
export class DeviceService implements DeviceServiceInterface {
  constructor(
    @inject(nameof<DeviceDataAccessInterface>())
    private _deviceDataAccess: DeviceDataAccessInterface
  ) {}

  async getByPhoneNumberAndUId(
    phoneNumber: string,
    uId: string
  ): Promise<Device | null> {
    return this._deviceDataAccess.getByPhoneNumberAndUId(phoneNumber, uId);
  }
}
