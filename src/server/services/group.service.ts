import { inject, injectable } from 'inversify';

import { DriverGroup, DriverGroupPermissionEnum } from '../models/driver-group';
import { Group } from '../models/group';
import { DriverGroupDataAccessInterface } from '../repository/driver-group.interface';
import { GroupDataAccessInterface } from '../repository/group.interface';
import { GroupServiceInterface } from './group.interface';

@injectable()
export class GroupService implements GroupServiceInterface {
  constructor(
    @inject(nameof<GroupDataAccessInterface>())
    private _groupDataAccess: GroupDataAccessInterface,
    @inject(nameof<DriverGroupDataAccessInterface>())
    private _driverGroupDataAccess: DriverGroupDataAccessInterface
  ) {}

  async create(group: Group): Promise<Group> {
    const createGroup = await this._groupDataAccess.create(group);
    // Add the driver and group to the DriverGroup table and set the permission to owner
    const driverGroup = new DriverGroup();
    driverGroup.driverId = createGroup.creatorId;
    driverGroup.groupId = createGroup.id;
    driverGroup.permission = DriverGroupPermissionEnum.OWNER;
    await this._driverGroupDataAccess.create(driverGroup);

    return createGroup;
  }

  async update(group: Group): Promise<[number]> {
    return this._groupDataAccess.update(group);
  }

  async deleteById(groupId: number): Promise<[number]> {
    return this._groupDataAccess.deleteById(groupId);
  }

  async getByGroupId(groupId: number): Promise<Group | null> {
    return this._groupDataAccess.getByGroupId(groupId);
  }

  async getByPublicId(publicId: string): Promise<Group | null> {
    return this._groupDataAccess.getByPublicId(publicId);
  }
}
