/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { ChildGroup } from '../models/child-group';
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { ChildGroupServiceInterface } from '../services/child-group.interface';
import { UserServiceInterface } from '../services/user.interface';
import { FirestoreServiceInterface } from '../services/firestore.interface';
import { Child } from '../models/child';
import { User } from '../models/user';
import { ChildServiceInterface } from '../services/child.interface';
import { DriverGroupServiceInterface } from '../services/driver-group.interface';
import { UserTypeEnum } from '../models/user-type.enum';

@injectable()
export class ChildGroupController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<ChildGroupServiceInterface>())
    private _childGroupService: ChildGroupServiceInterface,
    @inject(nameof<ChildServiceInterface>())
    private _childService: ChildServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface,
    @inject(nameof<DriverGroupServiceInterface>())
    private _driverGroupService: DriverGroupServiceInterface
  ) {
    this._error = errorFactory(nameof<ChildGroupController>());
  }

  async getByChildId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<ChildGroup>;
    res.json(
      await this._childGroupService.getByGroupId(
        request.validatedParams.childId
      )
    );
    next();
  }

  async getByGroupId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<ChildGroup>;
    res.json(
      await this._childGroupService.getByGroupId(
        request.validatedParams.groupId
      )
    );
    next();
  }

  async getByUserId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }
    res.json(await this._childGroupService.getByUserId(user.id));
    next();
  }

  async addChildsToGroup(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<ChildGroup> &
      RequestWithBody<ChildGroup>;
    res.json(
      await this._childGroupService.bulkCreate(
        request.validatedParams.groupId,
        request.validatedBody.childIds
      )
    );
    const data = await this._userService.getUserIdsByChildsIds(
      request.validatedBody.childIds
    );
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedParams.groupId
    );
    if (data && data.length) {
      await this._firestoreService.updateGroupTimestampByUserIds(
        data.map((i) => i.parentId)
      );
      await this._firestoreService.updateGroupTimestampByUserIds(
        driverGroup?.map((x) => x.driverId) ?? []
      );
    }
    next();
  }

  async addParentChildToGroup(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<ChildGroup> &
      RequestWithBody<Child>;

    let user = await this._userService.getUserByPhoneNumber(
      request.validatedBody.parentPhoneNumber!
    );

    if (!user) {
      const _user = new User();
      _user.fullName = request.validatedBody.parentFullName!;
      _user.phoneNumber = request.validatedBody.parentPhoneNumber!;
      _user.role = UserTypeEnum.PARENT;
      user = await this._userService.createUser(_user);
    }

    const _child = new Child();
    _child.fullName = request.validatedBody.fullName;
    _child.parentId = user.id;
    const child = await this._childService.create(_child);
    await this._childGroupService.bulkCreate(request.validatedParams.groupId, [
      child.id,
    ]);
    res.json(child);
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedParams.groupId
    );
    await Promise.all([
      this._firestoreService.updateGroupTimestampByUserIds([user.id]),
      this._firestoreService.updateGroupTimestampByUserIds(
        driverGroup?.map((x) => x.driverId) ?? []
      ),
    ]);

    next();
  }

  async removeChildFromGroup(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<ChildGroup> &
      RequestWithBody<ChildGroup>;
    res.json({
      updated: await this._childGroupService.deleteByGroupIdAndChildIds(
        request.validatedParams.groupId,
        request.validatedBody.childIds
      ),
    });
    const data = await this._userService.getUserIdsByChildsIds(
      request.validatedBody.childIds
    );
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedParams.groupId
    );
    if (data && data.length) {
      await this._firestoreService.updateGroupTimestampByUserIds(
        data.map((i) => i.parentId)
      );
      await this._firestoreService.updateGroupTimestampByUserIds(
        driverGroup?.map((x) => x.driverId) ?? []
      );
    }
    next();
  }

  async updateChildGroupMonthlyFees(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<ChildGroup>;
    res.json({
      updated: (
        await this._childGroupService.updateChildGroupMonthlyFees(
          request.validatedBody
        )
      )[0],
    });
    const data = await this._userService.getUserIdsByChildsIds([
      request.validatedBody.childId,
    ]);
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedBody.groupId
    );
    if (data && data.length) {
      await this._firestoreService.updateGroupTimestampByUserIds(
        data.map((i) => i.parentId)
      );
      await this._firestoreService.updateGroupTimestampByUserIds(
        driverGroup?.map((x) => x.driverId) ?? []
      );
    }
    next();
  }
}
