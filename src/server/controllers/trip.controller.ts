import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import moment from 'moment';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { ChildGroup } from '../models/child-group';
import { RequestWithBody, RequestWithParams, RequestWithPhoneNumber } from '../models/request-validation';
import { Trip } from '../models/trip';
import { ChildGroupServiceInterface } from '../services/child-group.interface';
import { CloudMessagingInterface } from '../services/cloud-messaging.interface';
import { DeviceServiceInterface } from '../services/device.interface';
import { DistanceServiceInterface } from '../services/distance.interface';
import { DriverGroupServiceInterface } from '../services/driver-group.interface';
import { PickupDropoffServiceInterface } from '../services/pickup-dropoff.interface';
import { TripServiceInterface } from '../services/trip.interface';
import { UserServiceInterface } from '../services/user.interface';
import { logger } from '../utils/logger';

@injectable()
export class TripController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<TripServiceInterface>())
    private _tripService: TripServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<DriverGroupServiceInterface>())
    private _driverGroupService: DriverGroupServiceInterface,
    @inject(nameof<PickupDropoffServiceInterface>())
    private _pickupDropoffService: PickupDropoffServiceInterface,
    @inject(nameof<DeviceServiceInterface>())
    private _deviceService: DeviceServiceInterface,
    @inject(nameof<ChildGroupServiceInterface>())
    private _childGroupService: ChildGroupServiceInterface,
    @inject(nameof<CloudMessagingInterface>())
    private _cloudMessagingService: CloudMessagingInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<DistanceServiceInterface>())
    private _distanceService: DistanceServiceInterface
  ) {
    this._error = errorFactory(nameof<TripController>());
  }

  async getByGroupId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Trip>;
    res.json(
      await this._tripService.getByGroupId(request.validatedParams.groupId)
    );
    next();
  }

  async start(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Trip> & RequestWithPhoneNumber;
    const device = await this._deviceService.getByPhoneNumberAndUId(
      request.validatedPhoneNumber,
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      request.validatedBody.uId!
    );
    if (!device) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find device by uId',
        errorData: {
          body: request.validatedBody,
        },
      });
    }
    const trip = request.validatedBody;
    trip.driverCurrentLatitude = trip.startLatitude;
    trip.driverCurrentLongitude = trip.startLongitude;
    trip.deviceId = device.id;
    trip.driverId = device.userId;
    trip.startTime = moment.utc().toDate();
    const tripStarted = await this._tripService.start(trip);
    res.json(tripStarted);
    if (device.user) {
      logger.info(
        'Sending push notification to all the parent devices for the groupId',
        { tripStarted }
      );
      await this._cloudMessagingService.startTripMessage(
        tripStarted.groupId,
        device.user.fullName
      );
    }
    next();
  }

  async end(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Trip> & RequestWithPhoneNumber;
    const trip = request.validatedBody;

    res.json({
      updated: (
        await this._tripService.end(
          trip.id,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          trip.endLatitude!,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          trip.endLongitude!
        )
      )[0],
    });
    // Dropoff all childs incase driver didn't do so
    const existingPickups = await this._pickupDropoffService.getAllPickupChildsByTripId(
      trip.id
    );

    const pickupDropoffIds = existingPickups.map((p) => p.id);
    if (pickupDropoffIds && pickupDropoffIds.length) {
      await this._pickupDropoffService.dropoffAllChildsByTripId(
        trip.id,
        pickupDropoffIds,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        trip.endLatitude!,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        trip.endLongitude!
      );
    }
    // const childIds = existingPickups.map((p) => p.childId);
    // if (childIds && childIds.length) {
    //   const user = await this._userService.getByPhoneNumber(
    //     request.validatedPhoneNumber
    //   );
    //   if (user && user.fullName) {
    //     await this._cloudMessagingService.dropoffChildsMessage(
    //       childIds,
    //       user.fullName
    //     );
    //   }
    // }
    logger.info(
      'Sending push notification to all the parent devices for the groupId',
      { tripId: trip.id }
    );
    await this._cloudMessagingService.endTripMessage(trip.id);

    next();
  }

  async getLatestTripsByUserId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const childs = await this._userService.getChildsByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!childs || !childs.length) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find childs by phoneNumber',
        errorData: {
          body: request.validatedPhoneNumber,
        },
      });
    }

    const childGroupsArray: Promise<ChildGroup[] | null>[] = [];

    childs.forEach((child) => {
      childGroupsArray.push(this._childGroupService.getByChildId(child.id));
    });

    const futures = await Promise.all(childGroupsArray);
    const childIds: number[] = [];

    const trips: Promise<Trip | null>[] = [];
    futures.forEach((childGroups) => {
      if (childGroups && childGroups.length) {
        childIds.push(childGroups[0].childId);
        trips.push(
          this._tripService.getLatestTripByGroupIds(
            childGroups.map((cg) => cg.groupId)
          )
        );
      }
    });

    const futures2 = await Promise.all(trips);
    futures2.forEach((trip, index) => {
      if (trip) {
        trip.setDataValue('childId', childIds[index]);
        trip.childId = childIds[index];
      }
    });
    res.json(await Promise.all(futures2));
    next();
  }

  async getLatestTripByDriverId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    res.json(await this._driverGroupService.getLatestTripByDriverId(user.id));
    next();
  }

  async updateTripDriverCurrentLocation(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithBody<Trip>;

    res.json({
      updated: (
        await this._tripService.updateTripDriverLocation(
          request.validatedBody.id,
          request.validatedBody.driverCurrentLatitude,
          request.validatedBody.driverCurrentLongitude
        )
      )[0],
    });
    next();
  }

  async updateActiveTripsDistanceAndDuration(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    res.json();
    await this._distanceService.calculateActiveTripDuration();
    next();
  }

  async endInactiveTrips(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    res.json({});
    await this._tripService.endInactiveTrips();
    next();
  }
}
