import { NextFunction, Request, Response } from "express";
import { inject, injectable } from "inversify";

import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { Group } from "../models/group";
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from "../models/request-validation";
import { GroupServiceInterface } from "../services/group.interface";
import { FirestoreServiceInterface } from "../services/firestore.interface";
import { UserServiceInterface } from "../services/user.interface";
import { DriverGroupServiceInterface } from "../services/driver-group.interface";
import randomstring from "randomstring";
import { ChildGroupServiceInterface } from "../services/child-group.interface";

@injectable()
export class GroupController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<GroupServiceInterface>())
    private _groupService: GroupServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<DriverGroupServiceInterface>())
    private _driverGroupService: DriverGroupServiceInterface,
    @inject(nameof<ChildGroupServiceInterface>())
    private _childGroupService: ChildGroupServiceInterface,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface
  ) {
    this._error = errorFactory(nameof<GroupController>());
  }

  async getByPublicId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Group>;
    res.json(
      await this._groupService.getByPublicId(request.validatedParams.publicId)
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Group> & RequestWithPhoneNumber;

    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: "extapi",
        status: 400,
        message: "Cannot find user by phoneNumber",
        errorData: {
          body: request.validatedBody,
        },
      });
    }

    const group = request.validatedBody;
    group.creatorId = user.id;
    group.publicId = randomstring.generate(5);
    const response = await this._groupService.create(group);
    res.json(response);
    await this._firestoreService.updateGroupTimestampByUserIds([user.id]);
    if (request.validatedBody.updateChildFees) {
      await this._childGroupService.bulkUpdateChildGroupMonthlyFees(
        response.id,
        request.validatedBody.monthFees,
        request.validatedBody.threeMonthsFees,
        request.validatedBody.sixMonthsFees,
        request.validatedBody.twelveMonthsFees
      );
    }
    next();
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Group>;
    res.json({
      updated: (await this._groupService.update(request.validatedBody))[0],
    });
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedBody.id
    );
    await this._firestoreService.updateGroupTimestampByUserIds(
      driverGroup?.map((x) => x.driverId) ?? []
    );
    if (request.validatedBody.updateChildFees) {
      await this._childGroupService.bulkUpdateChildGroupMonthlyFees(
        request.validatedBody.id,
        request.validatedBody.monthFees,
        request.validatedBody.threeMonthsFees,
        request.validatedBody.sixMonthsFees,
        request.validatedBody.twelveMonthsFees
      );
    }
    next();
  }

  async deleteByGroupId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Group>;
    const driverGroup = await this._driverGroupService.getByGroupId(
      request.validatedParams.id
    );
    res.json({
      deleted: (
        await this._groupService.deleteById(request.validatedParams.id)
      )[0],
    });
    await this._firestoreService.updateGroupTimestampByUserIds(
      driverGroup?.map((x) => x.driverId) ?? []
    );
    next();
  }
}
