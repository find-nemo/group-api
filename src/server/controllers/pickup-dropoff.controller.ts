import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import moment from 'moment';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { PickupDropoff } from '../models/pickup-dropoff';
import { RequestWithBody, RequestWithParams, RequestWithPhoneNumber } from '../models/request-validation';
import { CloudMessagingInterface } from '../services/cloud-messaging.interface';
import { PickupDropoffServiceInterface } from '../services/pickup-dropoff.interface';
import { UserServiceInterface } from '../services/user.interface';

@injectable()
export class PickupDropoffController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<PickupDropoffServiceInterface>())
    private _pickupDropoffService: PickupDropoffServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<CloudMessagingInterface>())
    private _cloudMessagingService: CloudMessagingInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<PickupDropoffController>());
  }

  async getByTripId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<PickupDropoff>;
    res.json(
      await this._pickupDropoffService.getByTripId(
        request.validatedParams.tripId
      )
    );
    next();
  }

  async pickupChild(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<PickupDropoff> &
      RequestWithPhoneNumber;
    const childIds = request.validatedBody.childIds;
    if (childIds && childIds.length && request.headers.authorization) {
      const user = await this._userService.getByPhoneNumber(
        request.validatedPhoneNumber
      );
      if (!user) {
        throw this._error.getFormattedError({
          source: 'extapi',
          status: 400,
          message: 'Cannot find user by phoneNumber',
          errorData: {
            body: request.validatedBody,
          },
        });
      }

      const pickupDropoffs: Partial<PickupDropoff>[] = [];
      childIds.forEach((childId) => {
        const pickupDropoff: Partial<PickupDropoff> = {
          childId,
          driverId: user.id,
          pickupTime: moment.utc().toDate(),
          tripId: request.validatedBody.tripId,
          pickupLatitude: request.validatedBody.pickupLatitude,
          pickupLongitude: request.validatedBody.pickupLongitude,
        };
        pickupDropoffs.push(pickupDropoff);
      });

      res.json(await this._pickupDropoffService.pickupChilds(pickupDropoffs));
      if (user && user.fullName) {
        if (childIds && childIds.length) {
          await this._cloudMessagingService.pickupChildsMessage(
            childIds,
            user.fullName
          );
        }
      }
      next();
    }
    throw this._error.getFormattedError({
      source: 'extapi',
      status: 400,
      message: 'Cannot find childsIds to pickup',
      errorData: {
        body: request.validatedBody,
      },
    });
  }

  async dropoffChild(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<PickupDropoff> &
      RequestWithPhoneNumber;
    const childIds = request.validatedBody.childIds;
    if (
      childIds &&
      childIds.length &&
      request.headers.authorization &&
      request.validatedBody.dropoffLatitude &&
      request.validatedBody.dropoffLongitude
    ) {
      res.json({
        updated: (
          await this._pickupDropoffService.dropoffChilds(
            childIds,
            request.validatedBody.tripId,
            request.validatedBody.dropoffLatitude,
            request.validatedBody.dropoffLongitude
          )
        )[0],
      });
      const user = await this._userService.getByPhoneNumber(
        request.validatedPhoneNumber
      );
      if (user && user.fullName) {
        await this._cloudMessagingService.dropoffChildsMessage(
          childIds,
          user.fullName
        );
      }
      next();
    }
    throw this._error.getFormattedError({
      source: 'extapi',
      status: 400,
      message: 'Cannot find childsIds to dropoff',
      errorData: {
        body: request.validatedBody,
      },
    });
  }

  async absentChild(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<PickupDropoff> &
      RequestWithPhoneNumber;
    const childIds = request.validatedBody.childIds;
    if (childIds && childIds.length && request.headers.authorization) {
      const user = await this._userService.getByPhoneNumber(
        request.validatedPhoneNumber
      );
      if (!user) {
        throw this._error.getFormattedError({
          source: 'extapi',
          status: 400,
          message: 'Cannot find user by phoneNumber',
          errorData: {
            body: request.validatedBody,
          },
        });
      }

      const pickupDropoffs: Partial<PickupDropoff>[] = [];
      childIds.forEach((childId) => {
        const pickupDropoff: Partial<PickupDropoff> = {
          childId,
          driverId: user.id,
          pickupTime: moment.utc().toDate(),
          tripId: request.validatedBody.tripId,
          isAbsent: true,
        };
        pickupDropoffs.push(pickupDropoff);
      });

      res.json(await this._pickupDropoffService.pickupChilds(pickupDropoffs));
      if (user && user.fullName) {
        if (childIds && childIds.length) {
          await this._cloudMessagingService.absentChildsMessage(
            childIds,
            user.fullName
          );
        }
      }
      next();
    }
    throw this._error.getFormattedError({
      source: 'extapi',
      status: 400,
      message: 'Cannot find childsIds to pickup',
      errorData: {
        body: request.validatedBody,
      },
    });
  }

  async notAbsentChild(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<PickupDropoff> &
      RequestWithPhoneNumber;
    const childIds = request.validatedBody.childIds;
    if (childIds && childIds.length && request.headers.authorization) {
      const user = await this._userService.getByPhoneNumber(
        request.validatedPhoneNumber
      );
      if (!user) {
        throw this._error.getFormattedError({
          source: 'extapi',
          status: 400,
          message: 'Cannot find user by phoneNumber',
          errorData: {
            body: request.validatedBody,
          },
        });
      }

      res.json({
        updated: await this._pickupDropoffService.revertAbsentMarkedChildsByTridId(
          request.validatedBody.tripId,
          childIds
        ),
      });
      if (user && user.fullName) {
        if (childIds && childIds.length) {
          await this._cloudMessagingService.notAbsentChildsMessage(
            childIds,
            user.fullName
          );
        }
      }
      next();
    }
    throw this._error.getFormattedError({
      source: 'extapi',
      status: 400,
      message: 'Cannot find childsIds to pickup',
      errorData: {
        body: request.validatedBody,
      },
    });
  }
}
