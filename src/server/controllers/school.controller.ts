import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { School } from '../models/school';
import { SchoolServiceInterface } from '../services/school.interface';
import { UserServiceInterface } from '../services/user.interface';

@injectable()
export class SchoolController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<SchoolServiceInterface>())
    private _schoolService: SchoolServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<SchoolController>());
  }

  async searchByName(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<School> & RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }
    res.json(
      await this._schoolService.searchByName(
        user.id,
        request.validatedParams.fullName,
        request.validatedParams.includeCreator === 'true' ? true : false,
        request.validatedParams.includeAddress === 'true' ? true : false
      )
    );
    next();
  }

  async searchById(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<School>;
    res.json(
      await this._schoolService.searchById(
        request.validatedParams.id,
        request.validatedParams.includeCreator === 'true' ? true : false,
        request.validatedParams.includeAddress === 'true' ? true : false
      )
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<School> & RequestWithPhoneNumber;

    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          body: request.validatedBody,
        },
      });
    }

    const school = request.validatedBody;
    school.createdBy = user.id;

    res.json(await this._schoolService.create(school));
    next();
  }
}
