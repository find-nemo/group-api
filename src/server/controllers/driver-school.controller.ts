import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import {
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { School } from '../models/school';
import { DriverGroupServiceInterface } from '../services/driver-group.interface';
import { SchoolServiceInterface } from '../services/school.interface';
import { UserServiceInterface } from '../services/user.interface';

@injectable()
export class DriverSchoolController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<DriverGroupServiceInterface>())
    private _driverGroupService: DriverGroupServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<SchoolServiceInterface>())
    private _schoolService: SchoolServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DriverSchoolController>());
  }

  async getDriverSchools(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<School> & RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    const driverGroups = await this._driverGroupService.getByDriverId(user.id);
    if (driverGroups && driverGroups.length) {
      const groupIds: number[] = [];
      for (const driverGroup of driverGroups) {
        groupIds.push(driverGroup.group.schoolId);
      }
      res.json(
        await this._schoolService.searchByIds(
          groupIds,
          request.validatedParams.includeCreator === 'true' ? true : false,
          request.validatedParams.includeAddress === 'true' ? true : false
        )
      );
    } else {
      res.json([]);
    }
    next();
  }
}
