import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { DriverGroup, DriverGroupPermissionEnum } from '../models/driver-group';
import { Group } from '../models/group';
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { User } from '../models/user';
import { UserType } from '../models/user-type';
import { DriverGroupServiceInterface } from '../services/driver-group.interface';
import { FirestoreServiceInterface } from '../services/firestore.interface';
import { GroupServiceInterface } from '../services/group.interface';
import { UserServiceInterface } from '../services/user.interface';

@injectable()
export class DriverGroupController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<DriverGroupServiceInterface>())
    private _driverGroupService: DriverGroupServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<GroupServiceInterface>())
    private _groupService: GroupServiceInterface,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DriverGroupController>());
  }

  async getByDriverId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }

    res.json(await this._driverGroupService.getByDriverId(user.id));
    next();
  }

  async getByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<User>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedParams.phoneNumber
    );

    if (!user) {
      throw this._error.getFormattedError({
        source: 'intdb',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedParams.phoneNumber,
        },
      });
    }
    const groups: Group[] = [];
    const driverGroups = await this._driverGroupService.getByDriverId(user.id);
    if (driverGroups && driverGroups.length > 0) {
      driverGroups.forEach((dg) => {
        groups.push(dg.group);
      });
    }

    res.json(groups);
    next();
  }

  async getDriversByGroupId(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<DriverGroup>;

    res.json(
      await this._driverGroupService.getByGroupId(
        request.validatedParams.groupId,
        true
      )
    );
    next();
  }

  async addDriverToGroup(
    req: Request,
    res: Response,
    next: Function
  ): Promise<void> {
    const request = req as RequestWithBody<DriverGroup>;

    let user = await this._userService.getUserByPhoneNumber(
      request.validatedBody.driverPhoneNumber!
    );

    if (!user) {
      const _user = new User();
      _user.fullName = request.validatedBody.driverFullName!;
      _user.phoneNumber = request.validatedBody.driverPhoneNumber!;
      user = await this._userService.createUser(_user);
      const _userType = new UserType();
      _userType.userId = user.id;
      _userType.isDriver = true;
      await this._userService.addUserType(_userType);
    }

    const _driverGroup = new DriverGroup();
    _driverGroup.groupId = request.validatedBody.groupId;
    _driverGroup.driverId = user.id;
    _driverGroup.permission = DriverGroupPermissionEnum.READ;
    res.json(await this._driverGroupService.findOrCreate(_driverGroup));
    await this._firestoreService.updateGroupTimestampByUserIds([user.id]);
    next();
  }

  async deleteDriverFromGroup(
    req: Request,
    res: Response,
    next: Function
  ): Promise<void> {
    const request = req as RequestWithParams<DriverGroup>;
    const group = await this._groupService.getByGroupId(
      request.validatedParams.groupId
    );

    if (!group || group.creatorId == request.validatedParams.driverId) {
      res.json({ deleted: 0 });
    } else {
      const driverGroup = await this._driverGroupService.getByGroupId(
        request.validatedParams.groupId
      );
      res.json({
        deleted: await this._driverGroupService.deleteDriverFromGroup(
          request.validatedParams.groupId,
          request.validatedParams.driverId
        ),
      });
      await this._firestoreService.updateGroupTimestampByUserIds(
        driverGroup?.map((x) => x.driverId)!
      );
    }

    next();
  }
}
