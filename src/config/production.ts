import { Config } from '../server/models/config';

export const productionConfig: Config = {
  serviceConfig: {
    name: 'Group API Microservice',
    environment: 'production',
    namespace: 'group-api',
    host: 'group-api.prod.bharatride.in',
    description: 'Template to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'ride-api-prod',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'productionTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.prod.bharatride.in',
    deviceApi: 'https://device-api.prod.bharatride.in',
  },
};
