import { Config } from '../server/models/config';

export const developmentConfig: Config = {
  serviceConfig: {
    name: 'Group API Microservice',
    environment: 'development',
    namespace: 'group-api',
    host: 'group-api.stg.bharatride.in',
    description: 'Template to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
