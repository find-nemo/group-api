import { Config } from '../server/models/config';

export const localConfig: Config = {
  serviceConfig: {
    name: 'Group API Microservice',
    environment: 'local',
    namespace: 'group-api',
    host: 'localhost',
    description: 'Template to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'local-db',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
