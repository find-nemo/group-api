import { Config } from '../server/models/config';

export const testConfig: Config = {
  serviceConfig: {
    name: 'Group API Microservice',
    environment: 'test',
    namespace: 'group-api',
    host: 'localhost',
    description: 'Template to be used for microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
      directionapikey: 'direction-api',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
